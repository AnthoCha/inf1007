module inf1007 {
    requires transitive javafx.controls;
	requires javafx.base;
	requires javafx.graphics;
	requires javafx.fxml;

    opens inf1007.app to javafx.fxml;
    
    exports inf1007.app;
    exports inf1007.bll;
    exports inf1007.bll.facades;
    exports inf1007.core.dtos;
    exports inf1007.core.entities;
    exports inf1007.core.enums;
    exports inf1007.core.interfaces;
}
