package inf1007.dal.uow;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * Regroupe les d�pots en m�moire centrale et persiste les entit�s dans un fichier binaire.
 * @author Anthony Chamberland
 *
 */
public class DatInMemoryUoW extends InMemoryUoW {

	private static final long serialVersionUID = 1L;
	
	private String dat;

	/**
	 * Cr�e un UnitOfWork en m�moire centrale qui persiste les entit�s dans le fichier binaire pr�cis�.
	 * @param dat Chemin d'acc�s du fichier binaire.
	 */
	public DatInMemoryUoW(String dat) {
		this.dat = dat;
	}

	/**
	 * Obtient une instance de DatInMemoryUoW � partir du fichier binaire pr�cis�.
	 * @param dat Chemin d'acc�s du fichier binaire.
	 * @return Une instance de DatInMemoryUoW
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public static DatInMemoryUoW fromDat(String dat) throws IOException, ClassNotFoundException {
		FileInputStream fin = new FileInputStream(dat);    
		ObjectInputStream in = new ObjectInputStream(fin);    
		DatInMemoryUoW uow = (DatInMemoryUoW)in.readObject();
		in.close();
		return uow;
	}

	/**
	 * @throws IOException
	 * @implNote Cette impl�mentation persiste les entit�s dans un fichier binaire.
	 */
	@Override
	public void saveChanges() throws IOException {
		FileOutputStream fout = new FileOutputStream(dat);    
		ObjectOutputStream out = new ObjectOutputStream(fout);    
		out.writeObject(this);    
		out.flush();    
		out.close();    
	}
}
