package inf1007.dal.uow;

import java.io.Serializable;

import inf1007.core.entities.ClientEntity;
import inf1007.core.entities.ReservationEntity;
import inf1007.core.entities.UtilisateurEntity;
import inf1007.core.entities.VehiculeEntity;
import inf1007.core.entities.TypeVehiculeEntity;
import inf1007.core.interfaces.Identity;
import inf1007.core.interfaces.Repository;
import inf1007.core.interfaces.UnitOfWork;
import inf1007.dal.identities.InMemoryIntIdentity;
import inf1007.dal.repositories.FromClientRepositoryReservationRepository;
import inf1007.dal.repositories.IdentityRepository;
import inf1007.dal.repositories.InMemoryRepository;

/**
 * Regroupe les d�pots en m�moire centrale.
 * @author Anthony Chamberland
 * @author Mathis Benny
 * @implNote Cette impl�mentation offre un lazy loading des d�pots.
 */
public class InMemoryUoW implements UnitOfWork, Serializable {

	private static final long serialVersionUID = 1L;
	
	private Repository<ClientEntity> client;
	private Repository<UtilisateurEntity> utilisateur;
	private Repository<VehiculeEntity> vehicule;
	private Repository<ReservationEntity> reservation;
	private Repository<TypeVehiculeEntity> typeVehicule;
	
	@Override
	public Repository<ClientEntity> getClient() {
		if (client == null) {
			Repository<ClientEntity> repository = new InMemoryRepository<ClientEntity>();
			Identity<Integer> identity = new InMemoryIntIdentity();
			client = new IdentityRepository<ClientEntity, Integer>(repository, identity);
		}
		
		return client;
	}

	@Override
	public Repository<UtilisateurEntity> getUtilisateur() {
		if (utilisateur == null) {
			Repository<UtilisateurEntity> repository = new InMemoryRepository<UtilisateurEntity>();
			Identity<Integer> identity = new InMemoryIntIdentity();
			utilisateur = new IdentityRepository<UtilisateurEntity, Integer>(repository, identity);
		}
		
		return utilisateur;
	}
	
	@Override
	public Repository<VehiculeEntity> getVehicule() {
		if (vehicule == null) {
			vehicule = new InMemoryRepository<VehiculeEntity>();
		}
		
		return vehicule;
	}
	
	@Override
	public Repository<ReservationEntity> getReservation() {
		if (reservation == null) {
			Repository<ClientEntity> clientRepository = getClient();
			Repository<ReservationEntity> repository = new FromClientRepositoryReservationRepository(clientRepository);
			Identity<Integer> identity = new InMemoryIntIdentity();
			reservation = new IdentityRepository<ReservationEntity, Integer>(repository, identity);
		}
		
		return reservation;
	}
	
	@Override
	public Repository<TypeVehiculeEntity> getTypeVehicule() {
		if(typeVehicule == null) {
			typeVehicule = new InMemoryRepository<TypeVehiculeEntity>();
		}
		
		return typeVehicule;
	}

	/**
	 * @throws Exception 
	 * @implNote Cette impl�mentation ne persiste aucun changements.
	 */
	@Override
	public void saveChanges() throws Exception {
		
	}


	
}
