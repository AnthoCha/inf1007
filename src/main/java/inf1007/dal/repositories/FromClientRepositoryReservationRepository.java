package inf1007.dal.repositories;

import java.io.Serializable;
import java.util.stream.Stream;

import inf1007.core.entities.ClientEntity;
import inf1007.core.entities.ReservationEntity;
import inf1007.core.interfaces.Repository;

/**
 * D�pot de r�servations d�duit du d�pot des clients.
 * Les r�servations doivent tout de m�me �tre ajout�e aux clients.
 * @author Anthony Chamberland
 *
 */
public class FromClientRepositoryReservationRepository implements Repository<ReservationEntity>, Serializable {

	private static final long serialVersionUID = 1L;
	
	private Repository<ClientEntity> clientRepository; 

	public FromClientRepositoryReservationRepository(Repository<ClientEntity> clientRepository) {
		this.clientRepository = clientRepository;
	}
	
	/**
	 * @apiNote La r�servation doit tout de m�me �tre ajout�e au client.
	 * @implNote Cette impl�mentation n'ajoute pas la r�servation au mod�le de persistance du client.
	 */
	@Override
	public void add(ReservationEntity entity) {

	}

	@Override
	public void remove(ReservationEntity entity) {
		clientRepository.stream()
		.filter(e -> e.getReservations().contains(entity))
		.findFirst()
		.ifPresent(e -> e.getReservations().remove(entity));
	}

	@Override
	public Stream<ReservationEntity> stream() {
		return clientRepository.stream()
				.flatMap(e -> e.getReservations().stream());
	}

}
