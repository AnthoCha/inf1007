package inf1007.dal.repositories;

import java.io.Serializable;
import java.util.stream.Stream;

import inf1007.core.interfaces.Identity;
import inf1007.core.interfaces.Identifiable;
import inf1007.core.interfaces.Repository;

/**
 * D�corateur de Repository qui d�finit l'identit� d'une entit� � son ajout.
 * @author Anthony Chamberland
 *
 * @param <T> Type de l'entit�.
 * @param <U> Type de l'identit�.
 */
public class IdentityRepository<T extends Identifiable<U>, U> implements Repository<T>, Serializable {

	private static final long serialVersionUID = 1L;
	
	private Repository<T> repository;
	private Identity<U> identity;
	
	public IdentityRepository(Repository<T> repository, Identity<U> identity) {
		this.repository = repository;
		this.identity = identity;
	}
	
	@Override
	public void add(T entity) {
		entity.setIdentity(identity.next());
		repository.add(entity);
	}
	
	@Override
	public void remove(T entity) {
		repository.remove(entity);
	}
	
	@Override
	public Stream<T> stream() {
		return repository.stream();
	}
}
