package inf1007.dal.repositories;

import java.util.LinkedList;
import java.io.Serializable;
import java.util.Collection;
import java.util.stream.Stream;

import inf1007.core.interfaces.Repository;

/**
 * Débot d'entités en mémoire.
 * @author Anthony Chamberland
 *
 * @param <T> Type de l'entité
 */
public class InMemoryRepository<T> implements Repository<T>, Serializable {

	private static final long serialVersionUID = 1L;
	
	private Collection<T> entities;
	
	public InMemoryRepository() {
		this.entities = new LinkedList<T>();
	}
	
	@Override
	public void add(T entity) {
		entities.add(entity);
	}

	@Override
	public void remove(T entity) {
		entities.remove(entity);
	}

	@Override
	public Stream<T> stream() {
		return entities.stream();
	}
}
