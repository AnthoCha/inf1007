package inf1007.dal.identities;

import java.io.Serializable;

import inf1007.core.interfaces.Identity;

/**
 * Identité de type entier en mémoire.
 * @author Anthony Chamberland
 *
 */
public class InMemoryIntIdentity implements Identity<Integer>, Serializable {

	private static final long serialVersionUID = 1L;
	
	private int identity, step;
	
	public InMemoryIntIdentity() {
		this(1, 1);
	}
	
	public InMemoryIntIdentity(int start, int step) {
		identity = start;
		this.step = step;
	} 
	
	@Override
	public Integer next() {
		return identity += step;
	}

}
