package inf1007.core.exceptions;

/**
 * Exception de validation
 * @author Anthony Chamberland
 *
 */
public class ValidationException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	/**
	 * Crée une exception de validation avec le message reçu en paramètre.
	 * @param message
	 */
	public ValidationException(String message) {
		super(message);
	}
	
}
