package inf1007.core.enums;

/**
 * Enum des raisons de retrait. 
 * @author Mathis Benny
 *
 */
public enum RaisonRetrait {
	ACCIDENT,
	REPARATION,
	AUTRE
}
