package inf1007.core.enums;

/**
 * Énumération des permissions de l'application
 * @author Anthony Chamberland
 *
 */
public enum Permissions {
	// Aucune permissions
	NONE(0),
	
	// Clients
	VIEW_CLIENTS(BasicPermissions.VIEW.value << ObjectPermissions.CLIENTS.shift),
	CREATE_CLIENTS(BasicPermissions.CREATE.value << ObjectPermissions.CLIENTS.shift),
	MODIFY_CLIENTS(VIEW_CLIENTS.value | (BasicPermissions.MODIFY.value << ObjectPermissions.CLIENTS.shift)),
	
	// V�hicules
	VIEW_VEHICULES(BasicPermissions.VIEW.value << ObjectPermissions.VEHICULES.shift),
	CREATE_VEHICULES(VIEW_VEHICULES.value | (BasicPermissions.CREATE.value << ObjectPermissions.VEHICULES.shift)),
	CREATE_RETRAITS(VIEW_VEHICULES.value | (BasicPermissions.DELETE.value << ObjectPermissions.VEHICULES.shift)),
	
	// R�servations
	VIEW_RESERVATIONS(VIEW_CLIENTS.value | VIEW_VEHICULES.value | (BasicPermissions.VIEW.value << ObjectPermissions.RESEVATIONS.shift)),
	CREATE_RESERVATIONS(VIEW_RESERVATIONS.value | (BasicPermissions.CREATE.value << ObjectPermissions.RESEVATIONS.shift)),
	MODIFY_RESERVATIONS(VIEW_RESERVATIONS.value | (BasicPermissions.MODIFY.value << ObjectPermissions.RESEVATIONS.shift)),
	DELETE_RESERVATIONS(MODIFY_RESERVATIONS.value | (BasicPermissions.DELETE.value << ObjectPermissions.RESEVATIONS.shift)),
	ALL_RESERVATIONS(VIEW_RESERVATIONS.value | CREATE_RESERVATIONS.value | MODIFY_RESERVATIONS.value | DELETE_RESERVATIONS.value),
	
	// Locations
	VIEW_LOCATIONS(VIEW_RESERVATIONS.value | (BasicPermissions.VIEW.value << ObjectPermissions.LOCATIONS.shift)),
	CREATE_LOCATIONS(VIEW_LOCATIONS.value | (BasicPermissions.CREATE.value << ObjectPermissions.LOCATIONS.shift)),
	MODIFY_LOCATIONS(VIEW_LOCATIONS.value | (BasicPermissions.MODIFY.value << ObjectPermissions.LOCATIONS.shift)),
	DELETE_LOCATIONS(MODIFY_LOCATIONS.value | (BasicPermissions.DELETE.value << ObjectPermissions.LOCATIONS.shift)),
	ALL_LOCATIONS(VIEW_LOCATIONS.value | CREATE_LOCATIONS.value | MODIFY_LOCATIONS.value | DELETE_LOCATIONS.value),
	
	// Factures
	VIEW_FACTURES(VIEW_LOCATIONS.value | (BasicPermissions.VIEW.value << ObjectPermissions.FACTURES.shift)),
	CREATE_FACTURES(VIEW_FACTURES.value | (BasicPermissions.CREATE.value << ObjectPermissions.FACTURES.shift)),
	MODIFY_FACTURES(VIEW_FACTURES.value | (BasicPermissions.MODIFY.value << ObjectPermissions.FACTURES.shift)),
	DELETE_FACTURES(MODIFY_FACTURES.value | (BasicPermissions.DELETE.value << ObjectPermissions.FACTURES.shift)),
	ALL_FACTURES(VIEW_FACTURES.value | CREATE_FACTURES.value | MODIFY_FACTURES.value | DELETE_FACTURES.value),

	// Retards
	VIEW_RETARDS(VIEW_FACTURES.value | (BasicPermissions.VIEW.value << ObjectPermissions.RETARDS.shift)),
	
	// Dossier
	VIEW_DOSSIER(BasicPermissions.VIEW.value << ObjectPermissions.DOSSIER.shift),

	// Conducteurs
	VIEW_CONDUCTEURS(VIEW_DOSSIER.value | (BasicPermissions.VIEW.value << ObjectPermissions.CONDUCTEURS.shift)),
	CREATE_CONDUCTEURS(VIEW_CONDUCTEURS.value | (BasicPermissions.CREATE.value << ObjectPermissions.CONDUCTEURS.shift)),
	MODIFY_CONDUCTEURS(VIEW_CONDUCTEURS.value | (BasicPermissions.MODIFY.value << ObjectPermissions.CONDUCTEURS.shift)),
	DELETE_CONDUCTEURS(MODIFY_CONDUCTEURS.value | (BasicPermissions.DELETE.value << ObjectPermissions.CONDUCTEURS.shift)),
	ALL_CONDUCTEURS(VIEW_CONDUCTEURS.value | CREATE_CONDUCTEURS.value | MODIFY_CONDUCTEURS.value | DELETE_CONDUCTEURS.value),
	
	// Rapports
	VIEW_RAPPORTS(BasicPermissions.VIEW.value << ObjectPermissions.RAPPORTS.shift),
	CREATE_RAPPORTS(VIEW_RAPPORTS.value | (BasicPermissions.CREATE.value << ObjectPermissions.RAPPORTS.shift)),

	// Offres
	VIEW_OFFRES(BasicPermissions.VIEW.value << ObjectPermissions.OFFRES.shift),
	
	// Syst�me
	START_SYSTEM(0b01 << ObjectPermissions.SYSTEM.shift),
	STOP_SYSTEM(0b01 << ObjectPermissions.SYSTEM.shift),
	
	// Impression
	PRINT(0b01 << ObjectPermissions.PRINT.shift),
	
	// Ensemble de permissions pr�d�finis par type d'utilisateur
	VISITEUR_PERMISSIONS(VIEW_OFFRES.value | CREATE_CLIENTS.value),
	CLIENT_PERMISSIONS(VIEW_OFFRES.value | VIEW_DOSSIER.value | VIEW_VEHICULES.value | ALL_CONDUCTEURS.value),
	PREPOSE_PERMISSIONS(MODIFY_CLIENTS.value | VIEW_VEHICULES.value | ALL_RESERVATIONS.value | ALL_LOCATIONS.value | ALL_FACTURES.value | VIEW_RETARDS.value | PRINT.value),
	MANAGER_PERMISSIONS(CREATE_RETRAITS.value | START_SYSTEM.value | STOP_SYSTEM.value | PRINT.value),
	GESTIONNAIRE_PERMISSIONS(CREATE_VEHICULES.value | CREATE_RAPPORTS.value | PRINT.value);
	
	private long value;
	
	private Permissions(long value) {
		this.value = value;
	}
	
	/**
	 * Valide que l'instance actuelle possède la permission reçu en paramètre.
	 * @param permission
	 * @return
	 */
	public boolean hasPermission(Permissions permission) {
		return (value & permission.value) == permission.value;
	}
	
	private enum BasicPermissions {
		VIEW(0b0001),
		CREATE(0b0010),
		MODIFY(0b0100),
		DELETE(0b1000);
		
		private long value;
		
		private BasicPermissions(long value) {
			this.value = value;
		}
	}
	
	private enum ObjectPermissions {
		CLIENTS(0),
		VEHICULES(4),
		RESEVATIONS(8),
		LOCATIONS(12),
		FACTURES(16),
		RETARDS(20),
		DOSSIER(24),
		CONDUCTEURS(28),
		RAPPORTS(32),
		OFFRES(36),
		SYSTEM(40),
		PRINT(42);
		
		private long shift;
		
		private ObjectPermissions(long shift) {
			this.shift = shift;
		}
	}
}
