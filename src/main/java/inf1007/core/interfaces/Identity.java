package inf1007.core.interfaces;

/**
 * Fournisseur d'identité.
 * @author Anthony Chamberland
 *
 * @param <T>Type de l'identité
 */
public interface Identity<T> {
	/**
	 * Obtient l'identité actuelle et itére vers la prochaine valeur.
	 * @return Value de l'identité
	 */
	T next();
}
