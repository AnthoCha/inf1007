package inf1007.core.interfaces;

/**
 * Fournisseur de sécurité.
 * @author Anthony Chamberland
 *
 */
public interface SecurityProvider {
	/**
	 * Obtient un salt.
	 * @return 
	 */
	byte[] getSalt();
	/**
	 * Obtient le hachage d'une chaine et de son salt.
	 * @param value Chaine
	 * @param salt Salt
	 * @return
	 */
	byte[] getHashWithSalt(String value, byte[] salt);
	/**
	 * Valide un hash
	 * @param value Chaine
	 * @param salt Salt
	 * @param hash Hash
	 * @return true si le hash est valide, sinon false.
	 */
	boolean validateHashWithSalt(String value, byte[] salt, byte[] hash);
}
