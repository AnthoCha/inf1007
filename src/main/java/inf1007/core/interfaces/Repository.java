package inf1007.core.interfaces;

import java.util.stream.Stream;

/**
 * D�pot d'un type d'entit�.
 * @author Anthony Chamberland
 *
 * @param <T> Type de l'entit�.
 */
public interface Repository<T> {
	/**
	 * Ajoute l'entit� au d�pot.
	 * @param entity Entit� a ajout�.
	 * @return L'identit� affect� � l'entit�.
	 */
	void add(T entity);
	
	/**
	 * Retire l'entit� du d�pot.
	 * @param entity Entit� a retir�.
	 */
	void remove(T entity);
	
	/**
	 * Obtient le flux d'entit�s.
	 * @return Le flux d'entit�s.
	 */
	Stream<T> stream();
}
