package inf1007.core.interfaces;

/**
 * Fournisseur de hachage.
 * @author Anthony Chamberland
 *
 */
public interface HashProvider {
	/**
	 * Applique une fonction de hachage sur les octets reçu en paramètres.
	 * @param bytes
	 * @return
	 */
	byte[] hash(byte[] bytes);
}
