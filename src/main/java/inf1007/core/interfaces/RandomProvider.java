package inf1007.core.interfaces;

/**
 * Fournisateur de données aléatoires.
 * @author Anthony Chamberland
 *
 */
public interface RandomProvider {
	/**
	 * Remplit le tableau reçu paramètre d'octets aléatoires.
	 * @param bytes
	 */
	void nextBytes(byte[] bytes);
}
