package inf1007.core.interfaces;

/**
 * Interface pour permettre la réception d'une valeur d'identité.
 * @author Anthony Chamberland
 *
 * @param <T> Type de l'identité
 */
public interface Identifiable<T> {
	/**
	 * Setter de l'identité.
	 * @param identity Valeur de l'identité
	 */
	void setIdentity(T identity);
}
