package inf1007.core.interfaces;

/**
 * Interface pour permettre la conversion d'une instance d'un object
 * dans le type de la classe DTO pr�cis�e.
 * @author Anthony Chamberland
 *
 * @param <T> Le type de la classe DTO.
 */
public interface ToDto<T> {
	/**
	 * Convertit l'instance actuelle dans le type de la classe DTO.
	 * @return Une instance de la classe DTO.
	 */
	T toDto();
}
