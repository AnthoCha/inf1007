package inf1007.core.interfaces;

import inf1007.core.entities.ClientEntity;
import inf1007.core.entities.ReservationEntity;
import inf1007.core.entities.TypeVehiculeEntity;
import inf1007.core.entities.UtilisateurEntity;
import inf1007.core.entities.VehiculeEntity;

/**
 * Regroupe les d�pots et permet la persistence des entit�s.
 * @author Anthony Chamberland
 * @author Mathis Benny
 *
 */
public interface UnitOfWork {
	/**
	 * Obtient le d�pot de clients.
	 * @return Le d�pot de clients.
	 */
	Repository<ClientEntity> getClient();
	
	/**
	 * Obtient le d�pot d'utilisateurs.
	 * @return Le d�pot d'utilisateurs.
	 */
	Repository<UtilisateurEntity> getUtilisateur();
	
	/**
	 * Obtient le d�pot de v�hicules.
	 * @return Le d�pot de v�hicules.
	 */
	Repository<VehiculeEntity> getVehicule();
	
	/**
	 * Obtient le d�pot de r�servations.
	 * @return Le d�pot de r�servations.
	 */
	Repository<ReservationEntity> getReservation();
	
	/**
	 * Obtient le dépot de types de véhicule.
	 * @return le dépot de types de véhicule.
	 */
	Repository<TypeVehiculeEntity> getTypeVehicule();
		
	
	/**
	 * Persiste les entit�s.
	 * @throws Exception Un erreur est survenue lors de la persistance des entit�s.
	 */
	void saveChanges() throws Exception;

}
