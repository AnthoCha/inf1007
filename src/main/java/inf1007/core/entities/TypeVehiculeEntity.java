package inf1007.core.entities;

import inf1007.core.interfaces.ToDto;
import inf1007.core.dtos.TypeVehiculeDto;

import java.io.Serializable;

/**
 * Entity des types de véhicules. 
 * @author Mathis Benny
 *
 */

public class TypeVehiculeEntity implements ToDto<TypeVehiculeDto>, Serializable {

	private static final long serialVersionUID = 1L;
	
	private String coteClassification, nom;

	public String getCoteClassification() {
		return coteClassification;
	}

	public void setCoteClassification(String coteClassification) {
		this.coteClassification = coteClassification;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}
	
	/**
	 * Permet de retourner le dto d'une TypeVehiculeEntity
	 */
	@Override
	public TypeVehiculeDto toDto() {
		TypeVehiculeDto dto = new TypeVehiculeDto();
		dto.setCoteClassification(coteClassification);
		dto.setNom(nom);
		return dto;
	}
}