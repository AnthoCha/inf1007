package inf1007.core.entities;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

import inf1007.core.dtos.ReservationDto;
import inf1007.core.interfaces.Identifiable;
import inf1007.core.interfaces.ToDto;
import inf1007.util.LocalDateTimeComparer;

public class ReservationEntity implements Identifiable<Integer>, ToDto<ReservationDto>, Serializable {

	private static final long serialVersionUID = 1L;
	
	private int noReservation;
	private float fCout;
	private LocalDateTime startDate, endDate;
	private LocationEntity location;
	private VehiculeEntity vehicule;
	
	public ReservationEntity(VehiculeEntity vehicule, LocalDateTime startDate, LocalDateTime endDate) {
		this.vehicule = vehicule;
		this.startDate = startDate;
		this.endDate = endDate;
	}
	
	public int getNoReservation() {
		return noReservation;
	}

	public LocalDateTime getStartDate() {
		return startDate;
	}

	public void setStartDate(LocalDateTime value) {
		startDate = value;
	}

	public LocalDateTime getEndDate() {
		return endDate;
	}

	public void setEndDate(LocalDateTime value) {
		endDate = value;
	}
	
	public LocationEntity getLocation() {
		return location;
	}

	public void setLocation(LocationEntity location) {
		this.location = location;
	}

	public VehiculeEntity getVehicule() {
		return vehicule;
	}

	public void setVehicule(VehiculeEntity value) {
		vehicule = value;
	}
	
	
	/**
	 * �value si le v�hicule pr�cis� est r�serv� par la r�servation � la date et heure actuelle.
	 * @param vehicule V�hicule
	 * @return true si le v�hicule est reserv� par la r�servation, sinon false.
	 */
	public boolean isReserve(VehiculeEntity vehicule) {
		return isReserve(vehicule, LocalDateTime.now());
	}

	/**
	 * �value si le v�hicule pr�cis� est r�serv� par la r�servation � la date et heure pr�cis�e.
	 * @param vehicule V�hicule
	 * @param date Date et heure
	 * @return true si le v�hicule est reserv� par la r�servation, sinon false.
	 */
	public boolean isReserve(VehiculeEntity vehicule, LocalDateTime date) {
		LocalDateTimeComparer comparer = new LocalDateTimeComparer();

		return this.vehicule.equals(vehicule) &&
				comparer.isBetween(date, startDate, endDate);
	}

	/**
	 * �value si le v�hicule pr�cis� est r�serv� par la r�servation pour la date pr�cis�e compl�te.
	 * @param vehicule V�hicule
	 * @param date Date
	 * @return true si le v�hicule est reserv� par la r�servation, sinon false.
	 */
	public boolean isReserve(VehiculeEntity vehicule, LocalDate date) {
		LocalDateTime dateTime = date.atStartOfDay();

		return this.vehicule.equals(vehicule) &&
				startDate.isBefore(dateTime) && endDate.isAfter(dateTime.plusDays(1));
	}

	/**
	 * �value si le v�hicule pr�cis� est r�serv� par la r�servation entre les dates et heures pr�cis�es.
	 * @param vehicule V�hicule
	 * @param startDate Date et heure de d�but
	 * @param endDate Date et heure de fin
	 * @return true si le v�hicule est reserv� par la r�servation, sinon false.
	 */
	public boolean isReserve(VehiculeEntity vehicule, LocalDateTime startDate, LocalDateTime endDate) {
		LocalDateTimeComparer comparer = new LocalDateTimeComparer();

		return this.vehicule.equals(vehicule) &&
				(comparer.isBetween(startDate, this.startDate, this.endDate) ||
						comparer.isBetween(endDate, this.startDate, this.endDate));
	}
	
	/**
	 * Évalue si le véhicule est en retard par rapport à sa date de fin de réservation.
	 * @return true si le véhicule est en retard, sinon false.
	 */
	public boolean isEnRetard() {
		LocalDateTime today = LocalDateTime.now();
		return location != null && location.getRetour() == null &&  today.isAfter(endDate);
	}

	@Override
	public void setIdentity(Integer identity) {
		noReservation = identity;
	}

	@Override
	public ReservationDto toDto() {
		ReservationDto dto = new ReservationDto();
		dto.setNoReservation(noReservation);
		dto.setStartDate(startDate);
		dto.setEndDate(endDate);
		dto.setVehicule(vehicule.toDto());
		return dto;
	}

	public float getCout() {
		return fCout;
	}

	public void setCout(float fCout) {
		this.fCout = fCout;
	}
}