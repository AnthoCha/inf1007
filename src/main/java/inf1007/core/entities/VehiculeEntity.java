package inf1007.core.entities;

import java.io.Serializable;

import inf1007.core.dtos.VehiculeDto;
import inf1007.core.enums.Classification;
import inf1007.core.interfaces.ToDto;
/**
 * Entité des véhicules.
 * 
 * @author Anthony Chamberland
 * @author Mathis Benny
 * @author Manu Magré
 * 
 */
public class VehiculeEntity implements ToDto<VehiculeDto>, Serializable {

	private static final long serialVersionUID = 1L;
	
	private String noLocation;
	private RetraitEntity retrait;
	private TypeVehiculeEntity typeVehicule;
	private String registration;
	private Classification classification;
	private int mileage;
	private String color;
	private String model;
	private String brand;
	private int year;

	
	public VehiculeEntity(String noLocation, String registration, Classification classification, int mileage,
			String color, String model, String brand, int year, TypeVehiculeEntity typeVehicule) {
		this.noLocation = noLocation;
		this.registration = registration;
		this.classification = classification;
		this.mileage = mileage;
		this.color = color;
		this.model = model;
		this.brand = brand;
		this.year = year;
		this.typeVehicule = typeVehicule;
	}
	
	public String getNoLocation() {
		return noLocation;
	}
	
	public void setNoLocation(String value) {
		noLocation = value;
	}
	
	public RetraitEntity getRetrait() {
		return retrait;
	}
	
	public void setRetrait(RetraitEntity value) {
		retrait = value;
	}
	
	public TypeVehiculeEntity getTypeVehicule() {
		return typeVehicule;
	}

	public void setTypeVehicule(TypeVehiculeEntity typeVehicule) {
		this.typeVehicule = typeVehicule;
	}
	
	public String getRegistration() {
		return registration;
	}

	public void setRegistration(String registration) {
		this.registration = registration;
	}

	public Classification getClassification() {
		return classification;
	}

	public void setClassification(Classification classification) {
		this.classification = classification;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}
	
	public int getMileage() {
		return mileage;
	}

	public void setMileage(int mileage) {
		this.mileage = mileage;
	}


	/**
	 * �value si le v�hicule est en service, c'est-�-dire qu'il n'est pas retir�.
	 * @return true si le v�hicule est en service, sinon false.
	 */
	public boolean isEnService() {
		return retrait == null;
	}
	
	/**
	 * Permet de retourner le dto d'une VehiculeEntity
	 */
	@Override
	public VehiculeDto toDto() {
		VehiculeDto dto = new VehiculeDto();
		dto.setNoLocation(noLocation);
		dto.setRetrait(retrait == null ? null : retrait.toDto());
		dto.setBrand(brand);
		dto.setClassification(classification);
		dto.setColor(color);
		dto.setMileage(mileage);
		dto.setModel(model);
		dto.setRegistration(registration);
		dto.setTypeVehicule(typeVehicule == null ? null : typeVehicule.toDto());
		return dto;
	}

}
