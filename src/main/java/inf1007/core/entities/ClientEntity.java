package inf1007.core.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.LinkedList;
import java.util.stream.Collectors;

import inf1007.core.dtos.ClientDto;
import inf1007.core.interfaces.Identifiable;
import inf1007.core.interfaces.ToDto;

public class ClientEntity implements Identifiable<Integer>, ToDto<ClientDto>, Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private int noDossier;
	private String nom, prenom, adresse, tel, permis, carteBancaire;
	private UtilisateurEntity utilisateur;
	private Collection<ReservationEntity> reservations;
	
	public ClientEntity(UtilisateurEntity utilisateur) {
		this.utilisateur = utilisateur;
		reservations = new LinkedList<ReservationEntity>();
	}
	
	public int getNoDossier() {
		return noDossier;
	}
	
	public String getNom() {
		return nom;
	}

	public void setNom(String value) {
		nom = value;
	}
	
	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String value) {
		prenom = value;
	}
	
	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String value) {
		adresse = value;
	}
	
	public String getTel() {
		return tel;
	}

	public void setTel(String value) {
		tel = value;
	}
	
	public String getPermis() {
		return permis;
	}

	public void setPermis(String value) {
		permis = value;
	}
	
	public String getCarteBancaire() {
		return carteBancaire;
	}

	public void setCarteBancaire(String value) {
		carteBancaire = value;
	}
	
	public UtilisateurEntity getUtilisateur() {
		return utilisateur;
	}
	
	public Collection<ReservationEntity> getReservations() {
		return reservations;
	}

	@Override
	public void setIdentity(Integer identity) {
		noDossier = identity;
	}
	
	@Override
	public ClientDto toDto() {
		ClientDto dto = new ClientDto();
		dto.setNoDossier(noDossier);
		dto.setNom(nom);
		dto.setPrenom(prenom);
		dto.setAdresse(adresse);
		dto.setTel(tel);
		dto.setPermis(permis);
		dto.setCarteBancaire(carteBancaire);
		
		dto.setUtilisateur(utilisateur.toDto());
		dto.setReservations(reservations.stream()
				.map(e -> e.toDto())
				.collect(Collectors.toList()));
		
		return dto;
	}

}
