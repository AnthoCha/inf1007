package inf1007.core.entities;

import inf1007.core.dtos.RetraitDto;
import inf1007.core.interfaces.ToDto;
import inf1007.core.enums.RaisonRetrait;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Entity des retraits. 
 * @author Mathis Benny
 *
 */
public class RetraitEntity implements ToDto<RetraitDto>, Serializable {

	private static final long serialVersionUID = 1L;
	
	private RaisonRetrait raisonRetrait;
	private String autre, description;
	private LocalDateTime date;

	public RaisonRetrait getRaisonRetrait() {
		return raisonRetrait;
	}
	
	public void setRaisonRetrait(RaisonRetrait value) {
		raisonRetrait = value;
	}
	
	public String getAutre() {
		return autre;
	}
	
	public void setAutre(String value) {
		autre = value;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String value) {
		description = value;
	}
	
	public LocalDateTime getDate() {
		return date;
	}
	
	public void setDate(LocalDateTime value) {
		date = value;
	}
	
	/**
	 * value si le retrait comporte une raison autre.
	 * @return true si la raison de retrait du vhicule est  autre, sinon false.
	 */
	public boolean isAutre() {
		return raisonRetrait == RaisonRetrait.AUTRE;
	}
	
	/**
	 * Permet de retourner le dto d'une RetraitEntity
	 */
	@Override
	public RetraitDto toDto() {
		RetraitDto dto = new RetraitDto();
		dto.setAutre(autre);
		dto.setDate(date);
		dto.setDescription(description);
		dto.setRaisonRetrait(raisonRetrait);
		return dto;
	}

}
