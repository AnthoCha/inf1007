package inf1007.core.entities;

import java.io.Serializable;
import java.time.LocalDateTime;

import inf1007.core.dtos.RetourDto;
import inf1007.core.interfaces.ToDto;

/**
 * Entity des retours de location. 
 * @author Pierre Tremblay
 * @author Mathis Benny
 *
 */
public class RetourEntity implements ToDto<RetourDto>, Serializable {

	private static final long serialVersionUID = 1L;
	
	private LocalDateTime date;
	private float kilometrage;
	
	public LocalDateTime getDate() {
		return date;
	}

	public void setDate(LocalDateTime value) {
		date = value;
	}
	
	public float getKilometrage() {
		return kilometrage;
	}

	public void setKilometrage(float kilometrage) {
		this.kilometrage = kilometrage;
	}
	
	@Override
	public RetourDto toDto() {
		RetourDto dto = new RetourDto();
		dto.setKilometrage(kilometrage);
		dto.setDate(date);
		return dto;
	}	
}
