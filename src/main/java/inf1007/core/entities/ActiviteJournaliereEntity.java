package inf1007.core.entities;

import java.io.Serializable;

import inf1007.core.dtos.ActiviteJournaliereDto;
import inf1007.core.dtos.TypeVehiculeDto;
import inf1007.core.interfaces.ToDto;

/**
 * Entity des activités journalières. 
 * @author Mathis Benny
 *
 */
public class ActiviteJournaliereEntity implements ToDto<ActiviteJournaliereDto>, Serializable {

	private static final long serialVersionUID = 1L;
	
	private long entree;
	private long sortie;
	private TypeVehiculeDto typeVehicule;
	
	public ActiviteJournaliereEntity(long entree, long sortie) {
		this.entree = entree;
		this.sortie = sortie;
	}
	
	public long getEntree() {
		return entree;
	}
	
	public void setEntree(int value) {
		entree = value;
	}
	
	public long getSortie() {
		return sortie;
	}
	
	public void setSortie(int value) {
		sortie = value;
	}
	
	public TypeVehiculeDto getTypeVehicule() {
		return typeVehicule;
	}

	public void setTypeVehicule(TypeVehiculeDto typeVehicule) {
		this.typeVehicule = typeVehicule;
	}
	
	/**
	 * Permet de retourner le dto d'une ActiviteJournaliereEntity
	 */
	@Override
	public ActiviteJournaliereDto toDto() {
		ActiviteJournaliereDto dto = new ActiviteJournaliereDto();
		dto.setEntree(entree);
		dto.setSortie(sortie);
		dto.setTypeVehicule(typeVehicule);
		return dto;
	}
}