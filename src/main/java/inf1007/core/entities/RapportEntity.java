package inf1007.core.entities;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.LinkedList;

import inf1007.core.dtos.ActiviteJournaliereDto;
import inf1007.core.dtos.RapportDto;
import inf1007.core.dtos.VehiculeDto;
import inf1007.core.interfaces.ToDto;

/**
 * Entity du rapport. 
 * @author Mathis Benny
 *
 */
public class RapportEntity implements ToDto<RapportDto>, Serializable {

	private static final long serialVersionUID = 1L;

	private Collection<ActiviteJournaliereDto> activitesJournalieres;
	private Collection<VehiculeDto> vehiculesEnRetard, vehiculesHorsService, vehicules;
	private LocalDateTime date;

	public RapportEntity() {
		vehicules = new LinkedList<VehiculeDto>();
		vehiculesEnRetard = new LinkedList<VehiculeDto>();
		vehiculesHorsService = new LinkedList<VehiculeDto>();
	}

	public Collection<VehiculeDto> getVehiculesEnRetard() {
		return vehiculesEnRetard;
	}

	public void setVehiculesEnRetard(Collection<VehiculeDto> value) {
		vehiculesEnRetard = value;
	}

	public Collection<VehiculeDto> getVehiculesHorsService() {
		return vehiculesHorsService;
	}

	public void setVehiculesHorsService(Collection<VehiculeDto> value) {
		vehiculesHorsService = value;
	}

	public Collection<VehiculeDto> getVehicules() {
		return vehicules;
	}

	public void setVehicules(Collection<VehiculeDto> value) {
		vehicules = value;
	}

	public LocalDateTime getDate() {
		return date;
	}

	public void setDate(LocalDateTime date) {
		this.date = date;
	}
	
	public Collection<ActiviteJournaliereDto> getActivitesJournalieres() {
		return activitesJournalieres;
	}

	public void setActivitesJournalieres(Collection<ActiviteJournaliereDto> activitesJournalieres) {
		this.activitesJournalieres = activitesJournalieres;
	}

	/**
	 * Permet de retourner le dto d'une RapportEntity
	 */
	@Override
	public RapportDto toDto() {
		RapportDto dto = new RapportDto();
		dto.setVehicules(vehicules == null ? null : vehicules);
		dto.setVehiculesEnRetard(vehiculesEnRetard == null ? null : vehiculesEnRetard);
		dto.setVehiculesHorsService(vehiculesHorsService == null ? null : vehiculesHorsService);
		dto.setDate(date);
		dto.setActivitesJournalieres(activitesJournalieres == null ? null : activitesJournalieres);
		return dto;
	}
}