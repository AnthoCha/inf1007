package inf1007.core.entities;

import java.io.Serializable;
import java.time.LocalDateTime;
import inf1007.core.dtos.LocationDto;
import inf1007.core.interfaces.ToDto;

/**
 * Entity des locations. 
 * @author Pierre Tremblay
 * @author Mathis Benny
 *
 */
public class LocationEntity implements ToDto<LocationDto>, Serializable {

	private static final long serialVersionUID = 1L;
	
	private LocalDateTime startDate;
	private RetourEntity retour;

	public LocalDateTime getStartDate() {
		return startDate;
	}

	public void setStartDate(LocalDateTime value) {
		startDate = value;
	}
	
	public RetourEntity getRetour() {
		return retour;
	}

	public void setRetour(RetourEntity retour) {
		this.retour = retour;
	}	

	@Override
	public LocationDto toDto() {
		LocationDto dto = new LocationDto();
		dto.setStartDate(startDate);	
		dto.setRetour(retour.toDto());
		return dto;
	}
}