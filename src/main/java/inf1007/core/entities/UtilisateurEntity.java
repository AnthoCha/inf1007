package inf1007.core.entities;

import java.io.Serializable;

import inf1007.core.dtos.UtilisateurDto;
import inf1007.core.enums.Permissions;
import inf1007.core.interfaces.Identifiable;
import inf1007.core.interfaces.ToDto;

public class UtilisateurEntity implements Identifiable<Integer>, ToDto<UtilisateurDto>, Serializable {

	private static final long serialVersionUID = 1L;
	
	private int id;
	private String nomUtilisateur;
	private byte[] hash, salt;
	private Permissions permissions;
	
	public UtilisateurEntity() {
		permissions = Permissions.NONE;
	}
	
	public int getId() {
		return id;
	}
	
	public String getNomUtilisateur() {
		return nomUtilisateur;
	}
	
	public void setNomUtilisateur(String value) {
		nomUtilisateur = value;
	}
	
	public byte[] getHash() {
		return hash;
	}
	
	public void setHash(byte[] value) {
		hash = value;
	}
	
	public byte[] getSalt() {
		return salt;
	}
	
	public void setSalt(byte[] value) {
		salt = value;
	}
	
	public Permissions getPermissions() {
		return permissions;
	}
	
	public void setPermissions(Permissions value) {
		permissions = value;
	}

	@Override
	public void setIdentity(Integer identity) {
		id = identity;
	}

	@Override
	public UtilisateurDto toDto() {
		UtilisateurDto dto = new UtilisateurDto();
		dto.setNomUtilisateur(nomUtilisateur);
		dto.setPermissions(permissions);
		return dto;
	}
	
}
