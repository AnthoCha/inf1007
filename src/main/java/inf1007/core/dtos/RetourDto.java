package inf1007.core.dtos;

import java.time.LocalDateTime;

/**
 * Data transfert object des retours de location.
 * @author Pierre Tremblay 
 * @author Mathis Benny
 *
 */
public class RetourDto {
	private LocalDateTime date;
	private float kilometrage;
	
	public float getKilometrage() {
		return kilometrage;
	}

	public void setKilometrage(float value) {
		kilometrage = value;
	}

	public LocalDateTime getDate() {
		return date;
	}
	
	public void setDate(LocalDateTime value) {
		date = value;
	}	
}