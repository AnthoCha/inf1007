package inf1007.core.dtos;

import java.time.LocalDateTime;
import java.util.Collection;

/**
 * Data transfert object du rapport. 
 * @author Mathis Benny
 *
 */
public class RapportDto {
	private Collection<ActiviteJournaliereDto> activitesJournalieres;  
	private Collection<VehiculeDto> vehiculesEnRetard, vehiculesHorsService, vehicules;
	private LocalDateTime date;
	
	public Collection<VehiculeDto> getVehiculesEnRetard() {
		return vehiculesEnRetard;
	}
	
	public void setVehiculesEnRetard(Collection<VehiculeDto> value) {
		vehiculesEnRetard = value;
	}
	
	public Collection<VehiculeDto> getVehiculesHorsService() {
		return vehiculesHorsService;
	}
	
	public void setVehiculesHorsService(Collection<VehiculeDto> value) {
		vehiculesHorsService = value;
	}
	
	public Collection<VehiculeDto> getVehicules() {
		return vehicules;
	}
	
	public void setVehicules(Collection<VehiculeDto> value) {
		vehicules = value;
	}

	public LocalDateTime getDate() {
		return date;
	}

	public void setDate(LocalDateTime date) {
		this.date = date;
	}

	public Collection<ActiviteJournaliereDto> getActivitesJournalieres() {
		return activitesJournalieres;
	}

	public void setActivitesJournalieres(Collection<ActiviteJournaliereDto> activitesJournalieres) {
		this.activitesJournalieres = activitesJournalieres;
	}
}
