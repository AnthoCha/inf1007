package inf1007.core.dtos;

import java.time.LocalDateTime;

/**
 * Data transfert object des locations.
 * @author Pierre Tremblay 
 * @author Mathis Benny
 *
 */
public class LocationDto {
	private LocalDateTime startDate;
	private RetourDto retour;
	
	public LocalDateTime getStartDate() {
		return startDate;
	}
	
	public void setStartDate(LocalDateTime value) {
		startDate = value;
	}

	public RetourDto getRetour() {
		return retour;
	}

	public void setRetour(RetourDto retour) {
		this.retour = retour;
	}	
}