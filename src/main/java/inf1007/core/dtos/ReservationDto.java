package inf1007.core.dtos;

import java.time.LocalDateTime;

public class ReservationDto {
	private int noReservation;
	private LocalDateTime startDate, endDate;
	private LocationDto location;
	private VehiculeDto vehicule;
	
	public int getNoReservation() {
		return noReservation;
	}
	
	public void setNoReservation(int value) {
		noReservation = value;
	}
	
	public LocalDateTime getStartDate() {
		return startDate;
	}
	
	public void setStartDate(LocalDateTime value) {
		startDate = value;
	}
	
	public LocalDateTime getEndDate() {
		return endDate;
	}
	
	public void setEndDate(LocalDateTime value) {
		endDate = value;
	}
	
	public LocationDto getLocation() {
		return location;
	}

	public void setLocation(LocationDto location) {
		this.location = location;
	}
	
	public VehiculeDto getVehicule() {
		return vehicule;
	}
	
	public void setVehicule(VehiculeDto value) {
		vehicule = value;
	}
	
}