package inf1007.core.dtos;

import java.util.Collection;

public class ClientDto {
	private int noDossier;
	private String nom, prenom, adresse, tel, permis, carteBancaire;
	private UtilisateurDto utilisateur;
	private Collection<ReservationDto> reservations;
	
	public int getNoDossier() {
		return noDossier;
	}
	
	public void setNoDossier(int value) {
		noDossier = value;
	}
	
	public String getNom() {
		return nom;
	}

	public void setNom(String value) {
		nom = value;
	}
	
	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String value) {
		prenom = value;
	}
	
	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String value) {
		adresse = value;
	}
	
	public String getTel() {
		return tel;
	}

	public void setTel(String value) {
		tel = value;
	}
	
	public String getPermis() {
		return permis;
	}

	public void setPermis(String value) {
		permis = value;
	}
	
	public String getCarteBancaire() {
		return carteBancaire;
	}

	public void setCarteBancaire(String value) {
		carteBancaire = value;
	}

	public UtilisateurDto getUtilisateur() {
		return utilisateur;
	}
	
	public void setUtilisateur(UtilisateurDto value) {
		utilisateur = value;
	}

	public Collection<ReservationDto> getReservations() {
		return reservations;
	}
	
	public void setReservations(Collection<ReservationDto> value) {
		reservations = value;
	}
	
}
