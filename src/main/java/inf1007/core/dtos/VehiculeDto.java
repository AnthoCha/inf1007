package inf1007.core.dtos;

import java.io.Serializable;

import inf1007.core.enums.Classification;
/**
 * Data object transfect des véhicules.
 * 
 * @author Mathis Benny
 * @author Anthony Chamberland
 * @author Manu Magré
 *
 */
public class VehiculeDto implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String noLocation;
	private RetraitDto retrait;
	private TypeVehiculeDto typeVehicule;
	private String registration;
	private Classification classification;
	private int mileage;
	private String color;
	private String model;
	private String brand;
	private int year;
	

	public String getNoLocation() {
		return noLocation;
	}
	
	public void setNoLocation(String value) {
		noLocation = value;
	}
	
	public RetraitDto getRetrait() {
		return retrait;
	}
	
	public void setRetrait(RetraitDto value) {
		retrait = value;
	}
    
	
	public TypeVehiculeDto getTypeVehicule() {
		return typeVehicule;
	}

	public void setTypeVehicule(TypeVehiculeDto typeVehicule) {
		this.typeVehicule = typeVehicule;
	}

	public String getRegistration() {
		return registration;
	}

	public void setRegistration(String registration) {
		this.registration = registration;
	}

	public Classification getClassification() {
		return classification;
	}

	public void setClassification(Classification classification) {
		this.classification = classification;
	}

	public int getMileage() {
		return mileage;
	}

	public void setMileage(int mileage) {
		this.mileage = mileage;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}	
	
}