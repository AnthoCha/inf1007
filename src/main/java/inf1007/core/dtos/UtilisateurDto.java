package inf1007.core.dtos;

import inf1007.core.enums.Permissions;

public class UtilisateurDto {
	private int id;
	private String nomUtilisateur;
	private Permissions permissions;
	
	public int getId() {
		return id;
	}
	
	public void setId(int value) {
		id = value;
	}
	
	public String getNomUtilisateur() {
		return nomUtilisateur;
	}
	
	public void setNomUtilisateur(String value) {
		nomUtilisateur = value;
	}
	
	public Permissions getPermissions() {
		return permissions;
	}
	
	public void setPermissions(Permissions value) {
		permissions = value;
	}
	
}
