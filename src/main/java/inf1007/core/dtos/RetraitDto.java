package inf1007.core.dtos;

import inf1007.core.enums.RaisonRetrait;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Data transfert object des retraits. 
 * @author Mathis Benny
 *
 */
public class RetraitDto implements Serializable{
	private static final long serialVersionUID = 1L;
	private RaisonRetrait raisonRetrait;
	private String autre, description;
	private LocalDateTime date;
	
	public RaisonRetrait getRaisonRetrait() {
		return raisonRetrait;
	}
	
	public void setRaisonRetrait(RaisonRetrait value) {
		raisonRetrait = value;
	}
	
	public String getAutre() {
		return autre;
	}
	
	public void setAutre(String value) {
		autre = value;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String value) {
		description = value;
	}
	
	public LocalDateTime getDate() {
		return date;
	}
	
	public void setDate(LocalDateTime value) {
		date = value;
	}
}
