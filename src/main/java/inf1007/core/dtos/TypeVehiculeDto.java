package inf1007.core.dtos;

import java.io.Serializable;

/**
 * Data transfert object des types de véhicules. 
 * @author Mathis Benny
 *
 */
public class TypeVehiculeDto implements Serializable {
	private static final long serialVersionUID = 1L;
	private String coteClassification, nom;

	public String getCoteClassification() {
		return coteClassification;
	}

	public void setCoteClassification(String coteClassification) {
		this.coteClassification = coteClassification;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}
}