package inf1007.core.dtos;

import java.io.Serializable;

/**
 * Data transfert object des activités journalières. 
 * @author Mathis Benny
 *
 */
public class ActiviteJournaliereDto implements Serializable {
	private static final long serialVersionUID = 1L;
	private long entree;
	private long sortie;
	private TypeVehiculeDto typeVehicule;
	
	public long getEntree() {
		return entree;
	}
	
	public void setEntree(long value) {
		entree = value;
	}
	
	public long getSortie() {
		return sortie;
	}
	
	public void setSortie(long value) {
		sortie = value;
	}

	public TypeVehiculeDto getTypeVehicule() {
		return typeVehicule;
	}

	public void setTypeVehicule(TypeVehiculeDto typeVehicule) {
		this.typeVehicule = typeVehicule;
	}
}