package inf1007.app;


/* Par Pierre Tremblay
 * 
 */

import java.io.IOException;

import inf1007.bll.Context;
import inf1007.core.dtos.ClientDto;
import inf1007.core.dtos.ReservationDto;
import inf1007.core.dtos.UtilisateurDto;
import inf1007.core.dtos.VehiculeDto;
import inf1007.core.enums.Permissions;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;


import javafx.stage.Stage;

public class FactureController extends AuthorizedController {

	private ClientDto client;
	private ReservationDto reservation;
	private VehiculeDto vehicule;
	private float fCout;
    
    
    //Section declaration Infos client
    @FXML
    private TextField txtNoDossier;
    @FXML
    private TextField txtNom;
    @FXML
    private TextField txtPrenom;
    @FXML
    private TextField txtAdresse;
    @FXML
    private TextField txtNoPermis;
    @FXML
    private TextField txtNoTelephone;
   
    //Section declaration Infos vehicule
    @FXML
    private TextField txtNumLoc;
    @FXML
    private TextField txtPlaque;
    @FXML
    private TextField txtKm;
    @FXML
    private TextField txtNewKm;
    @FXML
    private TextField txtRetard;
    @FXML
    private TextField txtDepot;

    @FXML
    private TextField txtMontantBase;
    @FXML
    private TextField txtMontantCalc;
    
    
    //Commandes  
    @FXML
    private Button btnDemFacture;
    @FXML
    private Button btnRetourCalcul;
	@FXML
	private Button btnRetourMenu;

    /**
     * Voir les infos de base du v�hicule de location
     * 
     * @param null
     * 
     */    
    private void chargementReservation() {
        txtNumLoc.setText(vehicule.getNoLocation());
        
        txtPlaque.setText(vehicule.getRegistration());
      
        txtKm.setText(String.valueOf(vehicule.getMileage()));		
	}

    /**
     * Voir le profil client
     * 
     * @param null
     */ 
	private void chargementClient() {
	    txtNoDossier.setText(String.valueOf(client.getNoDossier()));
	  
	    txtNom.setText(client.getNom());
	  
	    txtPrenom.setText(client.getPrenom());
	  
	    txtAdresse.setText(client.getAdresse());
	  
	    txtNoPermis.setText(client.getPermis());
	  
	   	txtNoTelephone.setText(client.getTel());
    }
    
	
	/*
    * M�thode effectuant le calcul de la facture
    *
    * 
    * @param null
    * @throws Exception
    */
	private void calculFacture() throws Exception {
		
		fCout = Float.valueOf(txtMontantBase.getText()) + Float.valueOf(txtRetard.getText()) - Float.valueOf(txtDepot.getText());		
		txtMontantCalc.setText(String.valueOf(fCout));
	}
   
    /**
     * M�thode effectuant le retour de location
     *
     * 
     * @param null
     * @throws Exception
     */
	private void retourLocation() throws Exception {				    				
		context.getReservation().setRetourLocation(reservation.getNoReservation(), Integer.valueOf(txtNewKm.getText()), fCout);	
		btnRetourCalcul.setDisable(true);
	}
	
	
    /**
     * Bouton permettant d'effectuer le calcul ainsi que le retour en un trait
     * On ne permettra pas de retourner la location si le km n'a pas �t� mis.
     *
     * 
     * @param event
     * @throws Exception
     */
	@FXML
    private void btnRetourCalculAction(ActionEvent event) throws Exception {
    	
    	// verifier si km et cout sont ok
    	if (txtNewKm.getText() != "")
    	 {	
    		calculFacture();
    		retourLocation();
    	 }
    }
	
	
    /**
     * Bouton de retour au menu principal.
     * 
     * @param event
     * @throws IOException
     */
    @FXML
    private void btnRetourAction(ActionEvent event) throws IOException {
    	redirectToMenu();
    }
    
    /**
     * Initialisation du controlleur Facture dans lequel on commence avec les infos
     * d'une r�servation s�lectionn�e, pour lequel nous avons extrait �galement la voiture
     * de location, ainsi que du client.
     * 
     * @param stage, context, utilisateur, reservation, client, vehicule
     * @throws Exception
     */    
	public void initialize(Stage stage, Context context, UtilisateurDto utilisateur, ReservationDto reservation, ClientDto client, VehiculeDto vehicule) {
		super.initialize(stage, context, utilisateur);
		this.reservation = reservation;
		this.client = client;
		this.vehicule = vehicule;
		chargementReservation();
		chargementClient();
		initializePermissions();
	}
	
    /**
     * Initialisation des permissions de la Location pour les diff�rents utilisateurs
     * 
     * @param null   
     */	
	private void initializePermissions() {
		btnRetourCalcul.setManaged(utilisateur.getPermissions().hasPermission(Permissions.ALL_FACTURES));    	
	}

	
}
