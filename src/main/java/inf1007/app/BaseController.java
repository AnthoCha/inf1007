package inf1007.app;

import java.io.IOException;

import inf1007.bll.Context;
import inf1007.core.dtos.UtilisateurDto;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public abstract class BaseController {

	protected Stage stage;
	protected Context context;
    
    public void initialize(Stage stage, Context context) {
    	this.stage = stage;
    	this.context = context;
    }
    
    protected void redirectToMenu(UtilisateurDto utilisateur) throws IOException {
        FXMLLoader loader = new FXMLLoader(App.class.getResource("menu.fxml"));
        Parent parent = loader.load();
        
        MenuController controller = loader.getController();
        controller.initialize(stage, context, utilisateur);
        
        Scene scene = new Scene(parent);
        stage.setScene(scene);
    }
    
}
