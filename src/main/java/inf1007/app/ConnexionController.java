package inf1007.app;

import java.io.IOException;

import inf1007.core.dtos.UtilisateurDto;
import inf1007.core.enums.Permissions;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

public class ConnexionController extends BaseController {

    @FXML
    private Label labNomUtilisateur;
    @FXML
    private Label labMdp;
    @FXML
    private Label labError;
    @FXML
    private TextField txtNomUtilisateur;
    @FXML
    private PasswordField pwdMdp;
    @FXML
    private Button btnConnexion;
    @FXML
    private Hyperlink linkVisiteur;

    @FXML
    private void btnConnexionAction(ActionEvent event) throws IOException {
    	UtilisateurDto utilisateur = context.getUtilisateur().connexion(txtNomUtilisateur.getText(), pwdMdp.getText());
    	
    	if (utilisateur == null) {
    		labError.setText("Nom d'utilisateur ou mot de passe incorrecte.");
    		return;
    	}
    	
    	redirectToMenu(utilisateur);
    }

    @FXML
    private void linkVisiteurAction(ActionEvent event) throws IOException {
    	UtilisateurDto utilisateur = new UtilisateurDto();
    	utilisateur.setId(0);
    	utilisateur.setNomUtilisateur("Visiteur");
    	utilisateur.setPermissions(Permissions.VISITEUR_PERMISSIONS);
    	
    	redirectToMenu(utilisateur);
    }
	
}
