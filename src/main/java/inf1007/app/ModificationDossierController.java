package inf1007.app;

import inf1007.bll.Context;
import inf1007.core.dtos.ClientDto;
import inf1007.core.dtos.UtilisateurDto;
import inf1007.core.exceptions.ValidationException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.stage.Stage;

/**
 * Controleur du cas d'utilisation MaJDossierClient. 
 * @author Manu Magr�
 *
 */
public class ModificationDossierController extends AuthorizedController{

    @FXML
    private Button btnReturn;
    @FXML
    private Button btnSaveNewInfo;
    @FXML
    private ToggleGroup infoChoice;
    @FXML
    private Label labAdress;
    @FXML
    private Label labCarteBanquaire;
    @FXML
    private Label labClient;
    @FXML
    private Label labFirstName;
    @FXML
    private Label labName;
    @FXML
    private Label labPermis;
    @FXML
    private Label labPhone;
    @FXML
    private RadioButton radAdress;
    @FXML
    private RadioButton radCarteBanquaire;
    @FXML
    private RadioButton radFirstName;
    @FXML
    private RadioButton radName;
    @FXML
    private RadioButton radPermis;
    @FXML
    private RadioButton radPhone;
    @FXML
    private TextField txtNewInfo;
    
    private ClientDto client;
    
    /**
     * Sauvegarde la nouvelle info entr�e en fonction du 
     * radioButton choisi et modifie le clientDto
     * 
     * @param event
     * @throws Exception
     */
    @FXML
    void btnSaveNewInfo(ActionEvent event) throws Exception{
    	RadioButton selectedRadioButton = (RadioButton) infoChoice.getSelectedToggle();
    	String newInfo = txtNewInfo.getText();
    	
    	
    	if(newInfo == null || newInfo == "")
    	{
    		throw new ValidationException("Une valeur est requise.");
    	}
    	
    	//V�rifie quelle information changer
    	switch(selectedRadioButton.getId()) {
    	case "radName":
    		client.setNom(newInfo);
    		labName.setText(newInfo);
    		break;
    	case "radFirstName":
    		client.setPrenom(newInfo);
    		labFirstName.setText(newInfo);
    		break;
    	case "radAdress":
    		client.setAdresse(newInfo);
    		labAdress.setText(newInfo);
    		break;
    	case "radPermis":
    		client.setPermis(newInfo);
    		labPermis.setText(newInfo);
    		break;
    	case "radPhone":
    		client.setTel(newInfo);
    		labPhone.setText(newInfo);
    		break;
    	case "radCarteBanquaire":
    		client.setCarteBancaire(newInfo);
    		labCarteBanquaire.setText(newInfo);
    		break;
    	default:
    		break;
    	}
    }
    
    /**
     *change le clientEntity correpsondant au clientDto
     * retournes au menu principal
     * @param event
     * @throws Exception
     */
    @FXML
    void btnReturnAction(ActionEvent event) throws Exception{
    	context.getClient().changeClient(client);
    	redirectToMenu();
    }
    
    /**
     * Initialise les donn�es du controleur
     * 
     * @param stage
     * @param context
     * @param utilisateur
     * @param client
     */
    
    public void initialize(Stage stage, Context context, UtilisateurDto utilisateur, ClientDto client) {
    	super.initialize(stage, context, utilisateur);
    	this.client = client;
    	
    	initializeComponents();
    }
    
    /**
     * Initialise les valeurs des labels repr�sentant
     * les informations du client
     */
    public void initializeComponents() {
    	labClient.setText(client.getNom() + client.getPrenom());
    	labAdress.setText(client.getAdresse());
    	labName.setText(client.getNom());
    	labFirstName.setText(client.getPrenom());
    	labCarteBanquaire.setText(client.getCarteBancaire());
    	labPermis.setText(client.getPermis());
    	labPhone.setText(client.getTel());
    }
}