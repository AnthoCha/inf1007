package inf1007.app;

import java.io.IOException;

import inf1007.app.controls.ClientCellFactory;
import inf1007.app.controls.VehiculeCellFactory;
import inf1007.bll.Context;
import inf1007.core.dtos.ClientDto;
import inf1007.core.dtos.RapportDto;
import inf1007.core.dtos.ReservationDto;
import inf1007.core.dtos.UtilisateurDto;
import inf1007.core.dtos.VehiculeDto;
import inf1007.core.enums.Permissions;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.Tab;
import javafx.stage.Stage;

/**
 * 
 * @author Manu Magre
 * @author Mathis Benny
 * @author Pierre Tremblay
 * @author Anthony Chamberland
 *
 */
public class MenuController extends AuthorizedController {

    private ClientDto[] clients = new ClientDto[0];
    private VehiculeDto[] vehicules = new VehiculeDto[0];
    private RapportDto rapport = new RapportDto();

    @FXML
    private Button btnCreerClient;
    @FXML
    private Button btnDeconnexion;
    @FXML
    private Button btnGetRapport;
    @FXML
    private Button btnMajClient;
    @FXML
    private Button btnReservations;
    @FXML
    private Button btnLocations;
    @FXML
    private Button btnAddVehicule;
    @FXML
    private Button btnDeleteVehicule;
    @FXML
    private ListView<ClientDto> lsvClients;
    @FXML
    private ListView<ReservationDto> lsvReservations;
    @FXML
    private ListView<VehiculeDto> lsvVehicules;
    @FXML
    private Tab tabClients;
    @FXML
    private Tab tabVehicules;

    @FXML
    private void btnCreerClientAction(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader(App.class.getResource("createClient.fxml"));
        Parent parent = loader.load();

        CreateClientController controller = loader.getController();
        controller.initialize(stage, context, utilisateur);

        Scene scene = new Scene(parent);
        stage.setScene(scene);
    }

    @FXML
    private void btnDeconnexionAction(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader(App.class.getResource("connexion.fxml"));
        Parent parent = loader.load();

        ConnexionController controller = loader.getController();
        controller.initialize(stage, context);

        Scene scene = new Scene(parent);
        stage.setScene(scene);
    }

    /**
     * Appel le controleur ModificationDossierCLient avec le
     * client choisi dans la liste
     * 
     * @param event
     * @throws IOException
     */
    @FXML
    private void btnMajClientAction(ActionEvent event) throws IOException {
        if (lsvClients.getSelectionModel().getSelectedIndex() != -1) {
            ClientDto client = clients[lsvClients.getSelectionModel().getSelectedIndex()];

            FXMLLoader loader = new FXMLLoader(App.class.getResource("modificationDossierClient.fxml"));
            Parent parent = loader.load();

            ModificationDossierController controller = loader.getController();
            controller.initialize(stage, context, utilisateur, client);

            Scene scene = new Scene(parent);
            stage.setScene(scene);
        }
    }

    /**
     * appel le controleur CreateVehiculeController
     * 
     * @param event
     * @throws IOException
     */

    @FXML
    private void btnAddVehiculeAction(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader(App.class.getResource("ajoutVehicule.fxml"));
        Parent parent = loader.load();

        CreateVehiculeController controller = loader.getController();
        controller.initialize(stage, context, utilisateur);

        Scene scene = new Scene(parent);
        stage.setScene(scene);
    }

    /**
     * appel le controleur DeleteVehiculeController
     * 
     * @param event
     * @throws IOException
     */
    @FXML
    private void btnDeleteVehiculeAction(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader(App.class.getResource("retraitVehicule.fxml"));
        Parent parent = loader.load();

        DeleteVehiculeController controller = loader.getController();
        controller.initialize(stage, context, utilisateur);

        Scene scene = new Scene(parent);
        stage.setScene(scene);
    }

    @FXML
    private void btnGetRapportAction(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader(App.class.getResource("obtenirRapport.fxml"));
        Parent parent = loader.load();

        RapportController controller = loader.getController();
        controller.initialize(stage, context, utilisateur, rapport);

        Scene scene = new Scene(parent);
        stage.setScene(scene);

    }

    /**
     * Permet de voir l'interface de location en fonction du client sélectionné
     * 
     * @param event
     * @throws IOException
     */  
    @FXML
    private void btnLocationAction(ActionEvent event) throws IOException {
        if (lsvClients.getSelectionModel().getSelectedIndex() != -1) {
            ClientDto client = clients[lsvClients.getSelectionModel().getSelectedIndex()];

            FXMLLoader loader = new FXMLLoader(App.class.getResource("gestionLocation.fxml"));
            Parent parent = loader.load();

            LocationController controller = loader.getController();
            controller.initialize(stage, context, utilisateur, client);

            Scene scene = new Scene(parent);
            stage.setScene(scene);
        }
    }

    @FXML
    private void btnReservationsAction(ActionEvent event) throws IOException {
        if (lsvClients.getSelectionModel().getSelectedIndex() != -1) {
            ClientDto client = clients[lsvClients.getSelectionModel().getSelectedIndex()];

            FXMLLoader loader = new FXMLLoader(App.class.getResource("reservations.fxml"));
            Parent parent = loader.load();

            ReservationsController controller = loader.getController();
            controller.initialize(stage, context, utilisateur, client);

            Scene scene = new Scene(parent);
            stage.setScene(scene);
        }
    }

    @Override
    public void initialize(Stage stage, Context context, UtilisateurDto utilisateur) {
        super.initialize(stage, context, utilisateur);
        initializePermissions();
        initializeClients();
        intializeVehicules();
        try {
            initializeRapport();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void initializePermissions() {
        if (!utilisateur.getPermissions().hasPermission(Permissions.VIEW_CLIENTS)) {
            tabClients.getTabPane().getTabs().remove(tabClients);
        }

        if (!utilisateur.getPermissions().hasPermission(Permissions.VIEW_VEHICULES)) {
            tabVehicules.getTabPane().getTabs().remove(tabVehicules);
        }

        btnCreerClient.setManaged(utilisateur.getPermissions().hasPermission(Permissions.CREATE_CLIENTS));
        btnMajClient.setManaged(utilisateur.getPermissions().hasPermission(Permissions.MODIFY_CLIENTS));
        btnReservations.setManaged(utilisateur.getPermissions().hasPermission(Permissions.VIEW_RESERVATIONS));
        btnAddVehicule.setManaged(utilisateur.getPermissions().hasPermission(Permissions.CREATE_VEHICULES));
        btnDeleteVehicule.setManaged(utilisateur.getPermissions().hasPermission(Permissions.CREATE_RETRAITS));
        btnLocations.setManaged(utilisateur.getPermissions().hasPermission(Permissions.VIEW_LOCATIONS));
        btnGetRapport.setManaged(utilisateur.getPermissions().hasPermission(Permissions.CREATE_RAPPORTS));
    }

    private void initializeClients() {
        clients = context.getClient().getClients().toArray(new ClientDto[0]);
        lsvClients.setCellFactory(new ClientCellFactory());
        populateClients();
    }

    private void populateClients() {
        lsvClients.getItems().clear();
        lsvClients.getItems().addAll(clients);
    }

    private void intializeVehicules() {
        vehicules = context.getVehicule().getVehiculesEnService().toArray(new VehiculeDto[0]);
        lsvVehicules.setCellFactory(new VehiculeCellFactory());
        populateVehicules();
    }

    private void populateVehicules() {
        lsvVehicules.getItems().clear();
        lsvVehicules.getItems().addAll(vehicules);
    }

    private void initializeRapport() throws Exception {
        rapport = context.getRapport().createRapport();
    }

}
