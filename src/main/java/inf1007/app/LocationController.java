package inf1007.app;

/* Par Pierre Tremblay
 * 
 */

import java.io.IOException;

import inf1007.app.controls.ReservationCellFactory;

import inf1007.bll.Context;
import inf1007.core.dtos.ClientDto;
import inf1007.core.dtos.ReservationDto;
import inf1007.core.dtos.UtilisateurDto;
import inf1007.core.dtos.VehiculeDto;
import inf1007.core.enums.Permissions;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;

import javafx.stage.Stage;

public class LocationController extends AuthorizedController {

    private ClientDto client;
    private ReservationDto[] reservations = new ReservationDto[0];
    private VehiculeDto vehicule;

    // Section declaration Reservation
    @FXML
    private Button btnVoirDossierLoc;

    @FXML
    private ListView<ReservationDto> lsvReservations;

    // Section declaration Infos client
    @FXML
    private TextField txtNoDossier;
    @FXML
    private TextField txtNom;
    @FXML
    private TextField txtPrenom;
    @FXML
    private TextField txtAdresse;
    @FXML
    private TextField txtNoPermis;
    @FXML
    private TextField txtNoTelephone;
    @FXML
    private Button btnMajClient;

    // Section declaration Infos vehicule
    @FXML
    private TextField txtNumLoc;
    @FXML
    private TextField txtPlaque;
    @FXML
    private TextField txtKm;
    @FXML
    private TextField txtCouleur;
    @FXML
    private TextField txtMarque;
    @FXML
    private TextField txtAnnee;
    @FXML
    private TextField txtDateDep;
    @FXML
    private TextField txtDateRet;

    // Commandes
    @FXML
    private Button btnDemarrerLoc;
    @FXML
    private Button btnChngRes;
    @FXML
    private Button btnFicheDegats;
    @FXML
    private Button btnDemFacture;
    @FXML
    private Button btnRetourMenu;    
    @FXML
    private Label labFeedback;

    
    /**
     * Permet de voir les informations de la r�servation au niveau client et location
     * 
     * @param event
     * @throws IOException
     */      
    @FXML
    private void btnVoirDossierLocAction(ActionEvent event) throws IOException {
        if (lsvReservations.getSelectionModel().getSelectedIndex() != -1) {
            ReservationDto reservation = reservations[lsvReservations.getSelectionModel().getSelectedIndex()];

            context.getClient();
            client = context.getClient().getClient(client.getNoDossier());
            vehicule = reservation.getVehicule();
            chargementClient();
            chargementReservation(reservation);
        }
    }

    /**
     * Voir le profil du v�hicule de location selon la r�servation s�lectionn�e
     * 
     * @param null
     * 
     */        
    private void chargementReservation(ReservationDto reservation) {
        txtNumLoc.setText(vehicule.getNoLocation());

        txtPlaque.setText(vehicule.getRegistration());

        txtKm.setText(String.valueOf(vehicule.getMileage()));

        txtCouleur.setText(vehicule.getColor());

        txtMarque.setText(vehicule.getModel());

        txtAnnee.setText(String.valueOf(vehicule.getYear()));

        txtDateDep.setText(String.valueOf(reservation.getStartDate()));

        txtDateRet.setText((String.valueOf(reservation.getEndDate())));

    }


    /**
     * Voir le profil client en fonction du client s�lectionn�
     * 
     * @param null
     */    
    private void chargementClient() {
        txtNoDossier.setText(String.valueOf(client.getNoDossier()));

        txtNom.setText(client.getNom());

        txtPrenom.setText(client.getPrenom());

        txtAdresse.setText(client.getAdresse());

        txtNoPermis.setText(client.getPermis());

        txtNoTelephone.setText(client.getTel());
    }
    

    /**
     * D�marrage de la location en fonction de la r�servation s�lectionn�e
     * 
     * @param event
     * @throws Exception
     */    
    @FXML
    private void btnDemarrerLocAction(ActionEvent event) throws Exception {
        if (lsvReservations.getSelectionModel().getSelectedIndex() != -1) {

            ReservationDto reservation = reservations[lsvReservations.getSelectionModel().getSelectedIndex()];
            context.getReservation().setLocation(reservation.getNoReservation());
            labFeedback.setText("Location démarrée avec succès.");
        }
    }

    /**
     * D�marrage de la facture en fonction de la r�servation s�lectionn�e
     * 
     * @param event
     * @throws IOException
     */
    @FXML
    private void btnDemFactureAction(ActionEvent event) throws IOException {
        if (lsvReservations.getSelectionModel().getSelectedIndex() != -1) {

            ReservationDto reservation = reservations[lsvReservations.getSelectionModel().getSelectedIndex()];

            FXMLLoader loader = new FXMLLoader(App.class.getResource("facture.fxml"));
            Parent parent = loader.load();

            FactureController controller = loader.getController();
            controller.initialize(stage, context, utilisateur, reservation, client, vehicule);

            Scene scene = new Scene(parent);
            stage.setScene(scene);
        }
    }

    /**
     * Appel le controleur ModificationDossierCLient avec le
     * client choisi dans la liste
     * 
     * @param event
     * @throws IOException
     */
    @FXML
    private void btnMajClientAction(ActionEvent event) throws IOException {

        FXMLLoader loader = new FXMLLoader(App.class.getResource("modificationDossierClient.fxml"));
        Parent parent = loader.load();

        ModificationDossierController controller = loader.getController();
        controller.initialize(stage, context, utilisateur, client);

        Scene scene = new Scene(parent);
        stage.setScene(scene);
    }

    /**
     * Appel le controleur UpdatReservation avec la r�servation s�lectionn�e
     * dans le but de la modifier
     * 
     * @param event
     * @throws IOException
     */
    @FXML
    private void btnChngResAction(ActionEvent event) throws IOException {
        if (lsvReservations.getSelectionModel().getSelectedIndex() != -1) {
            ReservationDto reservation = reservations[lsvReservations.getSelectionModel().getSelectedIndex()];

            FXMLLoader loader = new FXMLLoader(App.class.getResource("updateReservation.fxml"));
            Parent parent = loader.load();

            UpdateReservationController controller = loader.getController();
            controller.initialize(stage, context, utilisateur, reservation);

            Scene scene = new Scene(parent);
            stage.setScene(scene);
        }
    }

    /**
     * Bouton de retour au menu principal.
     * 
     * @param event
     * @throws IOException
     */
    @FXML
    private void btnRetourAction(ActionEvent event) throws IOException {
        redirectToMenu();
    }

    /**
     * Initialisation du controlleur Location dans lequel on commence avec les infos
     * d'un client s�lectionn�
     * 
     * @param stage, context, utilisateur, client
     * @throws Exception
     */
    public void initialize(Stage stage, Context context, UtilisateurDto utilisateur, ClientDto client) {
        super.initialize(stage, context, utilisateur);
        this.client = client;
        chargementClient();
        initializePermissions();
        initializeReservations();
    }

    /**
     * Initialisation des permissions de la Location pour les diff�rents utilisateurs
     * 
     * @param null   
     */
    private void initializePermissions() {
        btnMajClient.setManaged(utilisateur.getPermissions().hasPermission(Permissions.MODIFY_LOCATIONS));
        btnChngRes.setManaged(utilisateur.getPermissions().hasPermission(Permissions.MODIFY_LOCATIONS));
        btnDemarrerLoc.setManaged(utilisateur.getPermissions().hasPermission(Permissions.CREATE_LOCATIONS));
        btnDemFacture.setManaged(utilisateur.getPermissions().hasPermission(Permissions.VIEW_FACTURES));
    }

    /**
     * Initialisation de la liste des r�servations du client
     * 
     * @param null
     */
    private void initializeReservations() {
        reservations = client.getReservations().toArray(new ReservationDto[0]);
        lsvReservations.setCellFactory(new ReservationCellFactory());
        populateReservations();
    }


    /**
     * initialisation et population des r�servation dur client
     * 
     * @param null
     */
    private void populateReservations() {
        lsvReservations.getItems().clear();
        lsvReservations.getItems().addAll(reservations);
    }

}
