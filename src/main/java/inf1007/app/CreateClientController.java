package inf1007.app;

import inf1007.core.exceptions.ValidationException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

public class CreateClientController extends AuthorizedController {

    @FXML
    private Label labError;
    @FXML
    private TextField txtNomUtilisateur;
    @FXML
    private TextField txtNom;
    @FXML
    private TextField txtPrenom;
    @FXML
    private TextField txtAdresse;
    @FXML
    private TextField txtTelephone;
    @FXML
    private TextField txtPermis;
    @FXML
    private TextField txtCarteBancaire;
    @FXML
    private PasswordField pwdMdp;
    @FXML
    private PasswordField pwdConfMdp;
    @FXML
    private Button btnConf;
    @FXML
    private Button btnRetour;
    
    @FXML
    private void btnConfAction(ActionEvent event) throws Exception {
    	if (!pwdMdp.getText().equals(pwdConfMdp.getText())) {
    		labError.setText("Veuillez bien confirmer votre mot de passe.");
    		return;
    	}
    	
    	try {
    		context.getClient().createClient(
    			txtNomUtilisateur.getText(),
    			pwdMdp.getText(),
    			txtNom.getText(),
    			txtPrenom.getText(),
    			txtAdresse.getText(),
    			txtTelephone.getText(),
    			txtPermis.getText(),
    			txtCarteBancaire.getText()
    		);
    	} catch (ValidationException ex) {
    		labError.setText(ex.getMessage());
    		return;
    	}
    	
    	redirectToMenu();
    }
    
    @FXML
    private void btnRetourAction(ActionEvent event) throws Exception {
    	redirectToMenu();
    }
    
}
