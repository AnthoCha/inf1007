package inf1007.app;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;

import inf1007.bll.Context;
import inf1007.core.interfaces.HashProvider;
import inf1007.core.interfaces.RandomProvider;
import inf1007.core.interfaces.SecurityProvider;
import inf1007.core.interfaces.UnitOfWork;
import inf1007.dal.uow.DatInMemoryUoW;
import inf1007.security.DefaultSecurityProvider;
import inf1007.security.MessageDigestHashProvider;
import inf1007.security.PseudoRandomProvider;

/**
 * JavaFX App
 */
public class App extends Application {

	private final String DAT = "uow.dat";
	
    @Override
    public void start(Stage stage) throws Exception {
        FXMLLoader loader = new FXMLLoader(App.class.getResource("connexion.fxml"));
        Parent parent = loader.load();
        
        ConnexionController controller = loader.getController();
        Context context = intializeContext();
        controller.initialize(stage, context);
        
        Scene scene = new Scene(parent, 640, 480);
        stage.setTitle("INF1007");
        stage.setScene(scene);
        stage.show();
    }

    private Context intializeContext() throws Exception {
    	UnitOfWork uow;
    	File f = new File(DAT);
    	if (f.exists()) {
    		uow = DatInMemoryUoW.fromDat(DAT);
    	} else {
        	uow = new DatInMemoryUoW(DAT);
    	}
    	
    	HashProvider hash = new MessageDigestHashProvider(MessageDigest.getInstance("SHA-256"));
    	RandomProvider random = new PseudoRandomProvider();
    	SecurityProvider security = new DefaultSecurityProvider(hash, random, 32, StandardCharsets.UTF_8);
    	
    	Context context = new Context(uow, security);
    	
    	if (!f.exists()) {
    		context.getUtilisateur().createPrepose("prepose", "inf1007");
    		context.getUtilisateur().createGestionnaire("gestionnaire", "inf1007");
    		context.getUtilisateur().createManager("manager", "inf1007");
    		context.getTypeVehicule().createTypeVehicule("123456", "Berline");
    	}
    	
    	return context;
	}
    
    public static void main(String[] args) {
        launch();
    }

}