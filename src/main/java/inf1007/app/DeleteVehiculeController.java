package inf1007.app;

import inf1007.app.controls.VehiculeCellFactory;
import inf1007.bll.Context;
import inf1007.core.dtos.UtilisateurDto;
import inf1007.core.dtos.VehiculeDto;
import inf1007.core.enums.RaisonRetrait;
import inf1007.core.exceptions.ValidationException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.stage.Stage;

/**
 * Controleur du cas d'utilisation Retirer Véhicule
 * @author Mathis Benny
 *
 */
public class DeleteVehiculeController extends AuthorizedController{
	private VehiculeDto[] vehicules = new VehiculeDto[0];
	
	@FXML 
	private Button btnRetour;
    @FXML
    private ListView<VehiculeDto> lsvVehicules;
    @FXML
    private RadioButton radAccidente;
    @FXML
    private RadioButton radAReparer;
    @FXML
    private RadioButton radAutre;
    @FXML
    private TextField txtAutre;
    @FXML
    private TextArea txaDescription;
    @FXML
    private Button btnConf;
    @FXML
    private ToggleGroup raisonRetrait;
    @FXML 
    private Label lblErreur;
    
    /**
     * Renvoie le dto du véhicule sélectionné dans la liste.
     * 
     * @return
     */
    private VehiculeDto getSelectedVehicule() {
    	if (lsvVehicules.getSelectionModel().getSelectedIndex() != -1) {
    		return vehicules[lsvVehicules.getSelectionModel().getSelectedIndex()];
    	}
    	
    	return null;
    }
    
    /**
     * Action du bouton de confirmation de retrait de véhicule, qui crée le retrait dans le système.
     * 
     * @param event
     * @throws Exception
     */
    @FXML
    private void btnConfAction(ActionEvent event) throws Exception {
    	try {
    		RadioButton selectedRadioButton = (RadioButton) raisonRetrait.getSelectedToggle();
    		
    		VehiculeDto selectedVehicule = getSelectedVehicule();
    		
    		if(selectedVehicule == null) {
    			throw new ValidationException("Vous devez sélectionner un véhicule.");
    		}
    		
    		RaisonRetrait raisonRetraitEntree = RaisonRetrait.REPARATION;
    		switch(selectedRadioButton.getId()) {
    			case "radAccidente" :
    				raisonRetraitEntree = RaisonRetrait.ACCIDENT;
    				break;
    			case "radAReparer" :
    				raisonRetraitEntree = RaisonRetrait.REPARATION;
    				break;
    			case "radAutre" :
    				raisonRetraitEntree = RaisonRetrait.AUTRE;
    				break;
    		}
    		context.getRetrait().createRetrait(selectedVehicule.getNoLocation(), raisonRetraitEntree, txtAutre.getText(), txaDescription.getText());
    	}catch(ValidationException e) {
    		lblErreur.setText(e.getMessage());
    		return;
    	}
    	
    	redirectToMenu();
    }
    
    /**
     * Bouton de retour au menu principal.
     * 
     * @param event
     * @throws Exception
     */
    @FXML
    private void btnRetourAction(ActionEvent event) throws Exception {
    	redirectToMenu();
    }
    
    /**
     * Initialise les différents composants du controleur. 
     */
    public void initialize(Stage stage, Context context, UtilisateurDto utilisateur) {
		super.initialize(stage, context, utilisateur);
		initializeVehicules();
	}
    
    /**
     * Initialise les véhicules dans la liste appropriée.
     */
    private void initializeVehicules() {
		vehicules = context.getVehicule().getVehiculesEnService().toArray(new VehiculeDto[0]);
		lsvVehicules.setCellFactory(new VehiculeCellFactory());
		populateVehicules();
    }
    
    /**
     * Rempli la liste appropriée des véhicles
     */
    private void populateVehicules() {
    	lsvVehicules.getItems().clear();
    	lsvVehicules.getItems().addAll(vehicules);
    }
	
}