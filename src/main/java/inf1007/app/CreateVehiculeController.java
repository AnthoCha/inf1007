package inf1007.app;

import inf1007.core.enums.Classification;
import inf1007.core.exceptions.ValidationException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;

/**
 * Controleur du cas d'utilisation EnregistrerVehicule. 
 * @author Manu Magr�
 *
 */
public class CreateVehiculeController extends AuthorizedController{
	
	    @FXML
	    private TextField txtModel;
	    @FXML
	    private TextField txtColor;
	    @FXML
	    private TextField txtImmatriculation;
	    @FXML
	    private TextField txtKilo;
	    @FXML
	    private TextField txtBrand;
	    @FXML
	    private TextField txtNoLocation;
	    @FXML
	    private RadioButton radPrestige;
	    @FXML
	    private RadioButton radSimple;
	    @FXML
	    private RadioButton radUtilitaire;
	    @FXML
	    private Button btnReturnMenu;
	    @FXML
	    private Button btnSaveMenu;
	    @FXML
	    private TextField txtYear;
	    @FXML
	    private Label labError;
	    @FXML
	    private ToggleGroup classification;
	    
	    /**
	     * prends les donn�es entr�es pour cr�er un v�hicule
	     * et les passe � la facadeV�hicule pour cr�er et sauvegarder un v�hicule
	     * 
	     * @param event
	     * @throws Exception
	     */
	    @FXML
	    private void btnSaveVehiculeAction(ActionEvent event) throws Exception {
	    	try {
	    		RadioButton selectedRadioButton = (RadioButton) classification.getSelectedToggle();
	    		Classification classificationEntre = Classification.SIMPLE;
	    		switch(selectedRadioButton.getId()) {
	    			case "radPrestige":
	    				classificationEntre = Classification.PRESTIGE;
	    				break;
	    			case "radSimple":
	    				classificationEntre = Classification.SIMPLE;
	    				break;
	    			case "radUtilitaire":
	    				classificationEntre = Classification.UTILITAIRE;
	    				break;
	    		}
	    		
	    		context.getVehicule().CreateVehicule(txtNoLocation.getText(),
			    					txtImmatriculation.getText(),
			    					classificationEntre,
			    					Integer.parseInt(txtKilo.getText()),
			    					txtColor.getText(),
			    					txtModel.getText(),
			    					txtBrand.getText(),
			    					Integer.parseInt(txtYear.getText()));
	    			
	    	}catch (ValidationException ex) {
	    		labError.setText(ex.getMessage());
	    		return;
	    	}
	    	
	    	redirectToMenu();
	    }
	    
	    /**
	     * Retourne au menu principal
	     * 
	     * @param event
	     * @throws Exception
	     */
	    @FXML
	    private void btnRetourAction(ActionEvent event) throws Exception {
	    	redirectToMenu();
	    }
	    
}
