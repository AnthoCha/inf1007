package inf1007.app;


import java.io.IOException;

import inf1007.app.controls.ActiviteJournaliereCellFactory;
import inf1007.app.controls.VehiculeCellFactory;
import inf1007.app.controls.VehiculeEtatCellFactory;
import inf1007.bll.Context;
import inf1007.core.dtos.ActiviteJournaliereDto;
import inf1007.core.dtos.RapportDto;
import inf1007.core.dtos.UtilisateurDto;
import inf1007.core.dtos.VehiculeDto;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.stage.Stage;

/**
 * Controleur du cas d'utilisation Obtenir Rapport
 * 
 * @author Mathis Benny
 *
 */
public class RapportController extends AuthorizedController {
	
	private RapportDto rapport;
	
	private VehiculeDto[] vehicules = new VehiculeDto[0];
	private VehiculeDto[] vehiculesEnRetard = new VehiculeDto[0];
	private VehiculeDto[] vehiculesHorsService = new VehiculeDto[0];
	private ActiviteJournaliereDto[] activitesJournalieres = new ActiviteJournaliereDto[0];
	
    @FXML
    private Button btnRetour;
    @FXML
    private ListView<ActiviteJournaliereDto> lsvActivitesJournalieres;
    @FXML
    private ListView<VehiculeDto> lsvVehicules;
    @FXML
    private ListView<VehiculeDto> lsvVehiculesEnRetard;
    @FXML
    private ListView<VehiculeDto> lsvVehiculesHorsService;


    /**
     * Initalise les différentes composants du controleur.
     * 
     * @param stage
     * @param context
     * @param utilisateur
     * @param rapport
     */
    public void initialize(Stage stage, Context context, UtilisateurDto utilisateur, RapportDto rapport) {
    	super.initialize(stage, context, utilisateur);
    	this.rapport = rapport;
    	intializeVehicules();
    	intializeVehiculesEnRetard();
    	intializeVehiculesHorsService();
    	intializeActivitesJournalieres();
    }
    
    /**
     * Initialise tous les véhicules.
     */
    private void intializeVehicules() {
    	vehicules = rapport.getVehicules().toArray(new VehiculeDto[0]);
    	lsvVehicules.setCellFactory(new VehiculeEtatCellFactory());
    	populateVehicules();
    }
    
    /**
     * Rempli la liste de véhicules avec les véhicules initialisés.
     */
    private void populateVehicules() {
    	lsvVehicules.getItems().clear();
    	lsvVehicules.getItems().addAll(vehicules);
    }
    
    /**
     * Initialise les véhicules en retard.
     */
    private void intializeVehiculesEnRetard() {
    	vehiculesEnRetard = rapport.getVehiculesEnRetard().toArray(new VehiculeDto[0]);
    	lsvVehiculesEnRetard.setCellFactory(new VehiculeCellFactory());
    	populateVehiculesEnRetard();
    }
    
    /**
     * Rempli la liste de véhicules en retard avec les véhicules en retard initialisés.
     */
    private void populateVehiculesEnRetard() {
    	lsvVehiculesEnRetard.getItems().clear();
    	lsvVehiculesEnRetard.getItems().addAll(vehiculesEnRetard);
    }
    
    /**
     * Initialise les véhicules hors service.
     */
    private void intializeVehiculesHorsService() {
    	vehiculesHorsService = rapport.getVehiculesHorsService().toArray(new VehiculeDto[0]);
    	lsvVehiculesHorsService.setCellFactory(new VehiculeCellFactory());
    	populateVehiculesHorsService();
    }
    
    /**
     * Rempli la liste de véhicules hors service avec les véhicules hors service initialisés.
     */
    private void populateVehiculesHorsService() {
    	lsvVehiculesHorsService.getItems().clear();
    	lsvVehiculesHorsService.getItems().addAll(vehiculesHorsService);
    }
    
    /**
     * Initialise les activités journalières.
     */
    private void intializeActivitesJournalieres() {
    	activitesJournalieres = rapport.getActivitesJournalieres().toArray(new ActiviteJournaliereDto[0]);
    	lsvActivitesJournalieres.setCellFactory(new ActiviteJournaliereCellFactory());
    	populateActivitesJournalieres();
    }
    
    /**
     * Rempli la liste de activités journalières avec les activités journalières initialisés.
     */
    private void populateActivitesJournalieres() {
    	lsvActivitesJournalieres.getItems().clear();
    	lsvActivitesJournalieres.getItems().addAll(activitesJournalieres);
    }
    
    /**
     * Bouton de retour au menu principal.
     * 
     * @param event
     * @throws IOException
     */
    @FXML
    private void btnRetourAction(ActionEvent event) throws IOException {
    	redirectToMenu();
    }
    
}
