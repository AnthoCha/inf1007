package inf1007.app;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import inf1007.app.controls.VehiculeCellFactory;
import inf1007.bll.Context;
import inf1007.core.dtos.ClientDto;
import inf1007.core.dtos.UtilisateurDto;
import inf1007.core.dtos.VehiculeDto;
import inf1007.core.exceptions.ValidationException;
import javafx.collections.ListChangeListener;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class CreateReservationController extends AuthorizedController {
	
	private ClientDto client;
	private VehiculeDto[] vehicules = new VehiculeDto[0];
	private LocalDate[] startDates = new LocalDate[0];
	private LocalDate[] endDates = new LocalDate[0];
	private LocalDateTime[] startDateTimes = new LocalDateTime[0];
	private LocalDateTime[] endDateTimes = new LocalDateTime[0];
	
	@FXML
    private Label labError;
    @FXML
    private TextField txtStartTime;
    @FXML
    private TextField txtEndTime;
    @FXML
    private ListView<VehiculeDto> lsvVehicules;
    @FXML
    private ListView<LocalDate> lsvStartDate;
    @FXML
    private ListView<LocalDate> lsvEndDate;
    @FXML
    private ListView<LocalTime> lsvStartTime;
    @FXML
    private ListView<LocalTime> lsvEndTime;
    @FXML
    private DatePicker dateStartDateStart;
    @FXML
    private DatePicker dateStartDateEnd;
    @FXML
    private DatePicker dateEndDateEnd;
    @FXML
    private Button btnConf;
    @FXML
    private Button btnRetour;
    
    @FXML
    private void lsvVehiculesEdit(ActionEvent event) throws Exception {
    	startDateAction();
    }
    
    @FXML
    private void dateStartDateStartAction(ActionEvent event) throws Exception {
    	startDateAction();
    }
    
    @FXML
    private void dateStartDateEndAction(ActionEvent event) throws Exception {
    	startDateAction();
    }
    
    @FXML
    private void startDateAction() {
    	VehiculeDto vehicule = getSelectedVehicule();
    	
    	if (vehicule != null) {
    		startDates = context.getReservation()
    				.getDatesDebutDisponibles(vehicule.getNoLocation(), dateStartDateStart.getValue(), dateStartDateEnd.getValue())
    				.toArray(new LocalDate[0]);
    		populateStartDate();
    	}
    	
    	lsvStartTime.getItems().clear();
    	lsvEndDate.getItems().clear();
    	lsvEndTime.getItems().clear();
    }
    
    @FXML
    private void lsvStartDateEdit(ActionEvent event) throws Exception {
    	startDateTimeAction();
    }
    
    @FXML
    private void txtStartTimeAction(ActionEvent event) throws Exception {
    	startDateTimeAction();
    }

    private void startDateTimeAction() {
    	VehiculeDto vehicule = getSelectedVehicule();
    	
    	if (vehicule != null) {
    		LocalDate startDate = getSelectedStartDate();
    		Duration duration = Duration.ofMinutes(Long.parseLong(txtStartTime.getText()));
    		
    		startDateTimes = context.getReservation()
    				.getHeuresDebutDisponibles(vehicule.getNoLocation(), startDate, duration)
    				.toArray(new LocalDateTime[0]);
    		populateStartTime();
    	}

    	lsvEndDate.getItems().clear();
    	lsvEndTime.getItems().clear();
    }
    
    @FXML
    private void lsvStartTimeEdit(ActionEvent event) throws Exception {
    	endDateAction();
    }
    
    @FXML
    private void dateEndDateEndAction(ActionEvent event) throws Exception {
    	endDateAction();
    }
    
    private void endDateAction() {
    	VehiculeDto vehicule = getSelectedVehicule();
    	
    	if (vehicule != null) {
    		LocalDateTime startDateTime = getSelectedStartDateTime();
    		dateEndDateEnd.setValue(startDateTime.toLocalDate().plusDays(14));
    		
    		endDates = context.getReservation()
    				.getDatesFinDisponibles(vehicule.getNoLocation(), startDateTime, dateEndDateEnd.getValue())
    				.toArray(new LocalDate[0]);
    		populateEndDate();
    	}

    	lsvEndTime.getItems().clear();
    }
    
    @FXML
    private void lsvEndDateEdit(ActionEvent event) throws Exception {
    	endDateTimeAction();
    }
    
    @FXML
    private void txtEndTimeAction(ActionEvent event) throws Exception {
    	endDateTimeAction();
    }
    
    private void endDateTimeAction() {
    	VehiculeDto vehicule = getSelectedVehicule();
    	
    	if (vehicule != null) {
    		LocalDateTime startDateTime = getSelectedStartDateTime();
    		LocalDate endDate = getSelectedEndDate();
    		Duration duration = Duration.ofMinutes(Long.parseLong(txtEndTime.getText()));
    		
    		endDateTimes = context.getReservation()
    				.getHeuresFinDisponibles(vehicule.getNoLocation(), startDateTime, endDate, duration)
    				.toArray(new LocalDateTime[0]);
    		populateEndTime();
    	}
    }
    
    @FXML
    private void lsvEndTimeEdit(ActionEvent event) throws Exception {
    	
    }
    
    @FXML
    private void btnConfAction(ActionEvent event) throws Exception {
    	VehiculeDto vehicule = getSelectedVehicule();
    	
    	if (vehicule == null) {
    		labError.setText("Veuillez sélectionner un véhicule.");
    		return;
    	}
    	
    	try {
        	context.getReservation().createReservation(
        			client.getNoDossier(),
        			vehicule.getNoLocation(),
        			getSelectedStartDateTime(),
        			getSelectedEndDateTime()
        	);
    	} catch (ValidationException ex) {
    		labError.setText(ex.getMessage());
    		return;
    	}
    	
    	redirectToMenu();
    }
    
    @FXML
    private void btnRetourAction(ActionEvent event) throws Exception {
    	redirectToMenu();
    }
    
    public void initialize(Stage stage, Context context, UtilisateurDto utilisateur, ClientDto client) {
		super.initialize(stage, context, utilisateur);
		this.client = client;
		initializeDateTimes();
		initializeVehicules();
		initializeListViewsEvents();
	}
    
    private void initializeVehicules() {
		vehicules = context.getVehicule().getVehiculesEnService().toArray(new VehiculeDto[0]);
		lsvVehicules.setCellFactory(new VehiculeCellFactory());
		populateVehicules();
    }
    
    private void initializeDateTimes() {
		dateStartDateStart.setValue(LocalDate.now());
		dateStartDateEnd.setValue(LocalDate.now().plusDays(14));
		dateEndDateEnd.setValue(LocalDate.now().plusDays(14));	
    }
    
    private void initializeListViewsEvents() {
    	lsvVehicules.getSelectionModel().getSelectedItems().addListener(new ListChangeListener<VehiculeDto>() {

			@Override
			public void onChanged(Change<? extends VehiculeDto> c) {
				startDateAction();
			}
    		
    	});
    	
    	lsvStartDate.getSelectionModel().getSelectedItems().addListener(new ListChangeListener<LocalDate>() {

			@Override
			public void onChanged(Change<? extends LocalDate> c) {
				startDateTimeAction();
			}
    		
    	});
    	
    	lsvStartTime.getSelectionModel().getSelectedItems().addListener(new ListChangeListener<LocalTime>() {

			@Override
			public void onChanged(Change<? extends LocalTime> c) {
				endDateAction();
			}
    		
    	});
    	
    	lsvEndDate.getSelectionModel().getSelectedItems().addListener(new ListChangeListener<LocalDate>() {

			@Override
			public void onChanged(Change<? extends LocalDate> c) {
				endDateTimeAction();
			}
    		
    	});
    }
    
    private void populateVehicules() {
    	lsvVehicules.getItems().clear();
    	lsvVehicules.getItems().addAll(vehicules);
    }
    
    private void populateStartDate() {
    	lsvStartDate.getItems().clear();
    	lsvStartDate.getItems().addAll(startDates);
    }
    

    private void populateStartTime() {
    	lsvStartTime.getItems().clear();
    	for (LocalDateTime dateTime : startDateTimes) {
            lsvStartTime.getItems().add(dateTime.toLocalTime());
    	}
    }

    private void populateEndDate() {
    	lsvEndDate.getItems().clear();
    	lsvEndDate.getItems().addAll(endDates);
    }
    
    private void populateEndTime() {
    	lsvEndTime.getItems().clear();
    	for (LocalDateTime dateTime : endDateTimes) {
        	lsvEndTime.getItems().add(dateTime.toLocalTime());
    	}
    }
    
    private VehiculeDto getSelectedVehicule() {
    	if (lsvVehicules.getSelectionModel().getSelectedIndex() != -1) {
    		return vehicules[lsvVehicules.getSelectionModel().getSelectedIndex()];
    	}
    	
    	return null;
    }
    
    private LocalDate getSelectedStartDate() {
    	if (lsvStartDate.getSelectionModel().getSelectedIndex() != -1) {
    		return startDates[lsvStartDate.getSelectionModel().getSelectedIndex()];
    	}
    	
    	return LocalDate.now();
    }

    private LocalDate getSelectedEndDate() {
    	if (lsvEndDate.getSelectionModel().getSelectedIndex() != -1) {
    		return endDates[lsvEndDate.getSelectionModel().getSelectedIndex()];
    	}
    	
    	return getSelectedStartDate();
    }
    
    private LocalDateTime getSelectedStartDateTime() {
    	if (lsvStartTime.getSelectionModel().getSelectedIndex() != -1) {
    		return startDateTimes[lsvStartTime.getSelectionModel().getSelectedIndex()];
    	}
    	
    	return LocalDateTime.now();
    }
    
    private LocalDateTime getSelectedEndDateTime() {
    	if (lsvEndTime.getSelectionModel().getSelectedIndex() != -1) {
    		return endDateTimes[lsvEndTime.getSelectionModel().getSelectedIndex()];
    	}
    	
    	return getSelectedStartDateTime();
    }
    
}
