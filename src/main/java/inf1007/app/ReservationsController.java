package inf1007.app;

import java.io.IOException;

import inf1007.app.controls.ReservationCellFactory;
import inf1007.bll.Context;
import inf1007.core.dtos.ClientDto;
import inf1007.core.dtos.ReservationDto;
import inf1007.core.dtos.UtilisateurDto;
import inf1007.core.enums.Permissions;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.stage.Stage;

public class ReservationsController extends AuthorizedController {

	private ClientDto client;
	private ReservationDto[] reservations = new ReservationDto[0];

	@FXML
	private Button btnCreer;
	@FXML
	private Button btnMaj;
	@FXML
	private Button btnSupprimer;
	@FXML
	private Button btnAddVehicule;
	@FXML
	private Button btnRetour;
	@FXML
	private ListView<ReservationDto> lsvReservations;

	@FXML
	private void btnCreerAction(ActionEvent event) throws IOException {
		FXMLLoader loader = new FXMLLoader(App.class.getResource("createReservation.fxml"));
		Parent parent = loader.load();

		CreateReservationController controller = loader.getController();
		controller.initialize(stage, context, utilisateur, client);

		Scene scene = new Scene(parent);
		stage.setScene(scene);
	}

	@FXML
	private void btnMajAction(ActionEvent event) throws IOException {
		if (lsvReservations.getSelectionModel().getSelectedIndex() != -1) {
			ReservationDto reservation = reservations[lsvReservations.getSelectionModel().getSelectedIndex()];

			FXMLLoader loader = new FXMLLoader(App.class.getResource("updateReservation.fxml"));
			Parent parent = loader.load();

			UpdateReservationController controller = loader.getController();
			controller.initialize(stage, context, utilisateur, reservation);

			Scene scene = new Scene(parent);
			stage.setScene(scene);
		}
	}

	@FXML
	private void btnSupprimerAction(ActionEvent event) throws Exception {
		if (lsvReservations.getSelectionModel().getSelectedIndex() != -1) {
			ReservationDto reservation = reservations[lsvReservations.getSelectionModel().getSelectedIndex()];

			context.getReservation().deleteReservation(reservation.getNoReservation());
			client = context.getClient().getClient(client.getNoDossier());
			reservations = client.getReservations().toArray(new ReservationDto[0]);
			populateReservations();
		}
	}

	@FXML
	private void btnRetourAction(ActionEvent event) throws IOException {
		redirectToMenu();
	}

	public void initialize(Stage stage, Context context, UtilisateurDto utilisateur, ClientDto client) {
		super.initialize(stage, context, utilisateur);
		this.client = client;
		initializePermissions();
		initializeReservations();
	}

	private void initializePermissions() {
		btnCreer.setManaged(utilisateur.getPermissions().hasPermission(Permissions.CREATE_RESERVATIONS));
		btnMaj.setManaged(utilisateur.getPermissions().hasPermission(Permissions.MODIFY_RESERVATIONS));
		btnSupprimer.setManaged(utilisateur.getPermissions().hasPermission(Permissions.DELETE_RESERVATIONS));
	}

	private void initializeReservations() {
		reservations = client.getReservations().toArray(new ReservationDto[0]);
		lsvReservations.setCellFactory(new ReservationCellFactory());
		populateReservations();
	}

	private void populateReservations() {
		lsvReservations.getItems().clear();
		lsvReservations.getItems().addAll(reservations);
	}

}
