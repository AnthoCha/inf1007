package inf1007.app.controls;

import inf1007.core.dtos.ReservationDto;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.util.Callback;

public class ReservationCellFactory implements Callback<ListView<ReservationDto>, ListCell<ReservationDto>> {

	@Override
	public ListCell<ReservationDto> call(ListView<ReservationDto> param) {
		return new ListCell<>(){
            @Override
            public void updateItem(ReservationDto reservation, boolean empty) {
                super.updateItem(reservation, empty);
                if (empty || reservation == null) {
                    setText(null);
                } else {
                    setText(String.format("%s: %s à %s", reservation.getNoReservation(), reservation.getStartDate(), reservation.getEndDate()));
                }
            }
        };
	}

}
