package inf1007.app.controls;

import inf1007.core.dtos.VehiculeDto;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.util.Callback;

/**
 * Format de cellule pour les données concernant l'état des véhicules.
 * 
 * @author Mathis Benny
 *
 */
public class VehiculeEtatCellFactory implements Callback<ListView<VehiculeDto>, ListCell<VehiculeDto>> {

	@Override
	public ListCell<VehiculeDto> call(ListView<VehiculeDto> param) {
		return new ListCell<>(){
            @Override
            public void updateItem(VehiculeDto vehicule, boolean empty) {
                super.updateItem(vehicule, empty);
                if (empty || vehicule == null) {
                    setText(null);
                } else {
                	 setText(String.format("%s: %s", vehicule.getNoLocation(), vehicule.getRetrait() == null ? "en service" : "Raison du retrait: " + vehicule.getRetrait().getRaisonRetrait().toString()));
                }
            }
        };
	}
}
