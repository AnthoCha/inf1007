package inf1007.app.controls;

import inf1007.core.dtos.VehiculeDto;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.util.Callback;

public class VehiculeCellFactory implements Callback<ListView<VehiculeDto>, ListCell<VehiculeDto>> {

	@Override
	public ListCell<VehiculeDto> call(ListView<VehiculeDto> param) {
		return new ListCell<>(){
            @Override
            public void updateItem(VehiculeDto vehicule, boolean empty) {
                super.updateItem(vehicule, empty);
                if (empty || vehicule == null) {
                    setText(null);
                } else {
                    setText(vehicule.getNoLocation());
                }
            }
        };
	}

}
