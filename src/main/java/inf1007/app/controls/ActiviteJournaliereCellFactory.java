package inf1007.app.controls;

import inf1007.core.dtos.ActiviteJournaliereDto;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.util.Callback;

/**
 * Format de cellule pour les données d'activités journalières.
 * 
 * @author Mathis Benny
 *
 */
public class ActiviteJournaliereCellFactory implements Callback<ListView<ActiviteJournaliereDto>, ListCell<ActiviteJournaliereDto>> {

	@Override
	public ListCell<ActiviteJournaliereDto> call(ListView<ActiviteJournaliereDto> param) {
		return new ListCell<>(){
            @Override
            public void updateItem(ActiviteJournaliereDto activiteJournaliere, boolean empty) {
                super.updateItem(activiteJournaliere, empty);
                if (empty || activiteJournaliere == null) {
                    setText(null);
                } else {
                    setText(String.format("%s; Entrées: %s, Sorties: %s",activiteJournaliere.getTypeVehicule().getNom() , activiteJournaliere.getEntree(), activiteJournaliere.getSortie()));
                }
            }
        };
	}
}
