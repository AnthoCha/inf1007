package inf1007.app.controls;

import inf1007.core.dtos.ClientDto;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.util.Callback;

public class ClientCellFactory implements Callback<ListView<ClientDto>, ListCell<ClientDto>> {

	@Override
	public ListCell<ClientDto> call(ListView<ClientDto> param) {
		return new ListCell<>(){
            @Override
            public void updateItem(ClientDto client, boolean empty) {
                super.updateItem(client, empty);
                if (empty || client == null) {
                    setText(null);
                } else {
                    setText(String.format("%s: %s, %s", client.getNoDossier(), client.getNom(), client.getPrenom()));
                }
            }
        };
	}

}
