package inf1007.app;

import java.io.IOException;

import inf1007.bll.Context;
import inf1007.core.dtos.UtilisateurDto;
import javafx.stage.Stage;

public abstract class AuthorizedController extends BaseController {

	protected UtilisateurDto utilisateur;
    
    public void initialize(Stage stage, Context context, UtilisateurDto utilisateur) {
    	super.initialize(stage, context);
    	this.utilisateur = utilisateur;
    }
    
    protected void redirectToMenu() throws IOException {
        redirectToMenu(utilisateur);
    }
    
}
