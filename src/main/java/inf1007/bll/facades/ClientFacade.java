package inf1007.bll.facades;

import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

import inf1007.core.dtos.ClientDto;
import inf1007.core.entities.ClientEntity;
import inf1007.core.entities.UtilisateurEntity;
import inf1007.core.enums.Permissions;
import inf1007.core.exceptions.ValidationException;
import inf1007.core.interfaces.SecurityProvider;
import inf1007.core.interfaces.UnitOfWork;

public class ClientFacade {
	private UnitOfWork uow;
	private SecurityProvider security;

	public ClientFacade(UnitOfWork uow, SecurityProvider security) {
		this.uow = uow;
		this.security = security;
	}
	
	public Collection<ClientDto> getClients() {
		return uow.getClient().stream()
				.map(e -> e.toDto())
				.collect(Collectors.toList());
	}
	
	public void changeClient(ClientDto clientDto) throws Exception {
		Optional<ClientEntity> client = uow.getClient().stream().filter(e -> e.getNoDossier() == clientDto.getNoDossier()).findFirst();
		
		if(client.isPresent()) {
			ClientEntity clientEntity = client.get();
			clientEntity.setNom(clientDto.getNom());
			clientEntity.setPrenom(clientDto.getPrenom());
			clientEntity.setAdresse(clientDto.getAdresse());
			clientEntity.setTel(clientDto.getTel());
			clientEntity.setPermis(clientDto.getPermis());
			clientEntity.setCarteBancaire(clientDto.getCarteBancaire());
				
			uow.saveChanges();
		}
	}
		
		
	public ClientDto getClient(int noDossier){
		Optional<ClientEntity> client = uow.getClient().stream().filter(e -> e.getNoDossier() == noDossier).findFirst();
		
		if(client.isPresent()) {
			return client.get().toDto();
		}

		return null;
	}

	public void createClient(String nomUtilisateur, String mdp, String nom, String prenom, 
			String adresse, String tel, String permis, String carteBancaire) throws Exception {
		nomUtilisateur = nomUtilisateur.trim();
		mdp = mdp.trim();
		nom = nom.trim();
		prenom = prenom.trim();
		adresse = adresse.trim();
		tel = tel.trim();
		permis = permis.trim();
		carteBancaire = carteBancaire.trim();

		if (nomUtilisateur.isEmpty()) {
			throw new ValidationException("Le nom d'utilisateur est requis.");
		}

		if (mdp.isEmpty()) {
			throw new ValidationException("Le mot de passe est requis.");
		}

		if (nom.isEmpty()) {
			throw new ValidationException("Le nom est requis.");
		}

		if (prenom.isEmpty()) {
			throw new ValidationException("Le pr�nom est requis.");
		}

		if (adresse.isEmpty()) {
			throw new ValidationException("L'adresse est requise.");
		}

		if (tel.isEmpty()) {
			throw new ValidationException("Le num�ro de t�l�phone est requis.");
		}

		if (permis.isEmpty()) {
			throw new ValidationException("Le num�ro de permis de conduire est requis.");
		}

		if (carteBancaire.isEmpty()) {
			throw new ValidationException("Le num�ro de carte bancaire est requis.");
		}

		byte[] salt = security.getSalt();
		byte[] hash = security.getHashWithSalt(mdp, salt);
		
		UtilisateurEntity utilisateur = new UtilisateurEntity();
		utilisateur.setNomUtilisateur(nomUtilisateur);
		utilisateur.setSalt(salt);
		utilisateur.setHash(hash);
		utilisateur.setPermissions(Permissions.CLIENT_PERMISSIONS);
		
		uow.getUtilisateur().add(utilisateur);
		
		ClientEntity client = new ClientEntity(utilisateur);
		client.setNom(nom);
		client.setPrenom(prenom);
		client.setAdresse(adresse);
		client.setTel(tel);
		client.setPermis(permis);
		client.setCarteBancaire(carteBancaire);
		
		uow.getClient().add(client);
		uow.saveChanges();
	}
	
}
