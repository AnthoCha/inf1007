package inf1007.bll.facades;

import java.util.Optional;

import inf1007.core.dtos.UtilisateurDto;
import inf1007.core.entities.UtilisateurEntity;
import inf1007.core.enums.Permissions;
import inf1007.core.exceptions.ValidationException;
import inf1007.core.interfaces.SecurityProvider;
import inf1007.core.interfaces.UnitOfWork;

public class UtilisateurFacade {
	private UnitOfWork uow;
	private SecurityProvider security;

	public UtilisateurFacade(UnitOfWork uow, SecurityProvider security) {
		this.uow = uow;
		this.security = security;
	}

	/**
	 * Connecte un utilisateur avec son nom d'utilisateur et mot de passe.
	 * @param nomUtilisateur Nom d'utilisateur
	 * @param mdp Mot de passe
	 * @return L'utilisateur connect� ou null si la connexion �choue.
	 */
	public UtilisateurDto connexion(String nomUtilisateur, String mdp) {
		Optional<UtilisateurEntity> optionalUtilisateur = uow.getUtilisateur().stream()
				.filter(e -> e.getNomUtilisateur().equals(nomUtilisateur))
				.filter(e -> security.validateHashWithSalt(mdp, e.getSalt(), e.getHash()))
				.findFirst();

		if (optionalUtilisateur.isEmpty()) {
			return null;
		}

		return optionalUtilisateur.get().toDto();
	}
	
	public void createPrepose(String nomUtilisateur, String mdp) throws Exception {
		createUtilisateur(nomUtilisateur, mdp, Permissions.PREPOSE_PERMISSIONS);
	}
	
	public void createGestionnaire(String nomUtilisateur, String mdp) throws Exception {
		createUtilisateur(nomUtilisateur, mdp, Permissions.GESTIONNAIRE_PERMISSIONS);
	}
	
	public void createManager(String nomUtilisateur, String mdp) throws Exception {
		createUtilisateur(nomUtilisateur, mdp, Permissions.MANAGER_PERMISSIONS);
	}
	
	private void createUtilisateur(String nomUtilisateur, String mdp, Permissions permissions) throws Exception {
		nomUtilisateur = nomUtilisateur.trim();
		mdp = mdp.trim();

		if (nomUtilisateur.isEmpty()) {
			throw new ValidationException("Le nom d'utilisateur est requis.");
		}

		if (mdp.isEmpty()) {
			throw new ValidationException("Le mot de passe est requis.");
		}

		byte[] salt = security.getSalt();
		byte[] hash = security.getHashWithSalt(mdp, salt);
		
		UtilisateurEntity utilisateur = new UtilisateurEntity();
		utilisateur.setNomUtilisateur(nomUtilisateur);
		utilisateur.setSalt(salt);
		utilisateur.setHash(hash);
		utilisateur.setPermissions(permissions);
		
		uow.getUtilisateur().add(utilisateur);
		uow.saveChanges();
	}
}
