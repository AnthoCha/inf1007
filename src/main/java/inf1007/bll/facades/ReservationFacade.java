package inf1007.bll.facades;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

import inf1007.core.entities.ClientEntity;
import inf1007.core.entities.LocationEntity;
import inf1007.core.entities.ReservationEntity;
import inf1007.core.entities.RetourEntity;
import inf1007.core.entities.VehiculeEntity;
import inf1007.core.exceptions.ValidationException;
import inf1007.core.interfaces.Repository;
import inf1007.core.interfaces.UnitOfWork;
import inf1007.util.stream.LocalDateTimeStream;

/**
 * Facade des logiques d'affaires des r�servations. 
 * @author Anthony Chamberland
 *
 *logique debut et retour de location
 * @author Pierre Tremblay
 */
public class ReservationFacade {
	private UnitOfWork uow;

	public ReservationFacade(UnitOfWork uow) {
		this.uow = uow;
	}

	/**
	 * Obtient les dates de d�but disponibles entre les dates pr�cis�es pour le v�hicule avec le num�ro de location pr�cis�.
	 * Retourne null si le v�hicule n'existe pas.
	 * @param noLocation Num�ro de location du v�hicule
	 * @param startDate Date de d�but 
	 * @param endDate Date de fin
	 * @return Les dates de d�but disponibles ou null si le v�hicule n'existe pas.
	 */
	public Collection<LocalDate> getDatesDebutDisponibles(String noLocation, LocalDate startDate, LocalDate endDate) {
		Optional<VehiculeEntity> optionalVehicule = uow.getVehicule().stream()
				.filter(e -> e.getNoLocation().equals(noLocation))
				.findFirst();

		if (optionalVehicule.isEmpty()) {
			return null;
		}

		VehiculeEntity vehicule = optionalVehicule.get();

		return startDate.datesUntil(endDate)
				.filter(d -> !uow.getReservation().stream().anyMatch(e -> e.isReserve(vehicule, d)))
				.collect(Collectors.toList());
	}

	/**
	 * Obtient les dates et heures de d�but disponibles pour le v�hicule avec le num�ro de location pr�cis� et la date de d�but pr�cis�e.
	 * Les dates et heures disponibles se situent entre 0h et 24h pour la date de d�but pr�cis�e.
	 * Retourne null si le v�hicule n'existe pas.
	 * @param noLocation Num�ro de location du v�hicule
	 * @param startDate Date de d�but
	 * @param step Dur�e des sauts
	 * @return Les dates et heures de d�but disponibles ou null si le v�hicule n'existe pas.
	 */
	public Collection<LocalDateTime> getHeuresDebutDisponibles(String noLocation, LocalDate startDate, Duration step) {
		Optional<VehiculeEntity> optionalVehicule = uow.getVehicule().stream()
				.filter(e -> e.getNoLocation().equals(noLocation))
				.findFirst();

		if (optionalVehicule.isEmpty()) {
			return null;
		}

		VehiculeEntity vehicule = optionalVehicule.get();

		LocalDateTime dateTime = startDate.atStartOfDay();
		return LocalDateTimeStream.range(dateTime, dateTime.plusDays(1), step)
				.filter(d -> !uow.getReservation().stream().anyMatch(e -> e.isReserve(vehicule, d)))
				.collect(Collectors.toList());
	}

	/**
	 * Obtient les dates de fin disponibles � partir de la date et heure de d�but pr�cis�e jusqu'� la date de fin pr�cis�e pour le v�hicule avec le num�ro de location pr�cis�.
	 * Retourne null si le v�hicule n'existe pas.
	 * @param noLocation Num�ro de location du v�hicule
	 * @param startDate Date et heure de d�but
	 * @param endDate Date de fin
	 * @return Les dates de fin disponibles ou null si le v�hicule n'existe pas.
	 */
	public Collection<LocalDate> getDatesFinDisponibles(String noLocation, LocalDateTime startDate, LocalDate endDate) {
		Optional<VehiculeEntity> optionalVehicule = uow.getVehicule().stream()
				.filter(e -> e.getNoLocation().equals(noLocation))
				.findFirst();

		if (optionalVehicule.isEmpty()) {
			return null;
		}

		VehiculeEntity vehicule = optionalVehicule.get();

		return startDate.toLocalDate().datesUntil(endDate)
				.takeWhile(d -> !uow.getReservation().stream().anyMatch(e -> e.isReserve(vehicule, startDate, d.atStartOfDay().isBefore(startDate) ? startDate : d.atStartOfDay())))
				.collect(Collectors.toList());
	}
	
	/**
	 * 
	 * Obtient les dates et heures de fin disponibles pour le v�hicule avec le num�ro de location pr�cis�
	 * pour la date et heure de d�but pr�cis�e jusqu'� et la date de fin pr�cis�e.
	 * Les dates et heures disponibles se situent entre 0h et 24h pour la date de fin pr�cis�e.
	 * Retourne null si le v�hicule n'existe pas.
	 * @param noLocation Num�ro de location du v�hicule
	 * @param startDate Date et heure de d�but
	 * @param endDate Date de fin
	 * @param step Dur�e des sauts
	 * @return Les dates et heures de fin disponibles ou null si le v�hicule n'existe pas.
	 */
	public Collection<LocalDateTime> getHeuresFinDisponibles(String noLocation, LocalDateTime startDate, LocalDate endDate, Duration step) {
		Optional<VehiculeEntity> optionalVehicule = uow.getVehicule().stream()
				.filter(e -> e.getNoLocation().equals(noLocation))
				.findFirst();

		if (optionalVehicule.isEmpty()) {
			return null;
		}

		VehiculeEntity vehicule = optionalVehicule.get();

		LocalDateTime dateTime = endDate.atStartOfDay().isBefore(startDate) ? startDate : endDate.atStartOfDay();
		return LocalDateTimeStream.range(dateTime, dateTime.plusDays(1), step)
				.takeWhile(d -> !uow.getReservation().stream().anyMatch(e -> e.isReserve(vehicule, startDate, d)))
				.collect(Collectors.toList());
	}
	
	/**
	 * Cr�e une r�servation pour le client avec le num�ro de dossier pr�cis�, le v�hicule avec le num�ro de location pr�cis� et les dates et heures de d�but et de fin pr�cis�es.
	 * @param noDossier Num�ro de dossier du client
	 * @param noLocation Num�ro de location du v�hicule
	 * @param startDate Date et heure de d�but de la r�servation
	 * @param endDate Date et heure de fin de la r�servation
	 * @throws Exception Un erreur est survenue lors de la cr�ation de la r�servation.
	 */
	public void createReservation(int noDossier, String noLocation, LocalDateTime startDate, LocalDateTime endDate) throws Exception {
		Optional<VehiculeEntity> optionalVehicule = uow.getVehicule().stream()
				.filter(e -> e.getNoLocation().equals(noLocation))
				.findFirst();

		if (optionalVehicule.isEmpty()) {
			throw new ValidationException("Le véhicule avec le numéro de location spécifié n'existe pas.");
		}

		Optional<ClientEntity> optionalClient = uow.getClient().stream()
				.filter(e -> e.getNoDossier() == noDossier)
				.findFirst();
		
		if (optionalClient.isEmpty()) {
			throw new ValidationException("Le client avec le numéro de dossier spécifié n'existe pas.");
		}
		
		VehiculeEntity vehicule = optionalVehicule.get();
		Repository<ReservationEntity> reservationRepository = uow.getReservation();
		
		if (reservationRepository.stream()
				.anyMatch(e -> e.isReserve(vehicule, startDate, endDate))) {
			throw new ValidationException("Le véhicule n'est pas disponibles pour les dates et heures spécifiés,");
		}
		
		ReservationEntity reservation = new ReservationEntity(vehicule, startDate, endDate);
		reservationRepository.add(reservation);
		
		ClientEntity client = optionalClient.get();
		client.getReservations().add(reservation);
		uow.saveChanges();
	}

	/**
	 * Mets à jour une r�servation avec le num�ro de réservation pr�cis�, le v�hicule avec le num�ro de location pr�cis� et les dates et heures de d�but et de fin pr�cis�es.
	 * @param noReservation Num�ro de réservation à mettre à jour
	 * @param noLocation Num�ro de location du v�hicule
	 * @param startDate Date et heure de d�but de la r�servation
	 * @param endDate Date et heure de fin de la r�servation
	 * @throws Exception Un erreur est survenue lors de la cr�ation de la r�servation.
	 */
	public void updateReservation(int noReservation, String noLocation, LocalDateTime startDate, LocalDateTime endDate) throws Exception {
		Optional<ReservationEntity> optionalReservation = uow.getReservation().stream()
				.filter(e -> e.getNoReservation() == noReservation)
				.findFirst();
		
		if (optionalReservation.isEmpty()) {
			throw new ValidationException("La réservation avec le numéro de réservation spécifiée n'existe pas.");
		}
		
		Optional<VehiculeEntity> optionalVehicule = uow.getVehicule().stream()
				.filter(e -> e.getNoLocation().equals(noLocation))
				.findFirst();

		if (optionalVehicule.isEmpty()) {
			throw new ValidationException("Le véhicule avec le numéro de location spécifié n'existe pas.");
		}
		
		VehiculeEntity vehicule = optionalVehicule.get();
		Repository<ReservationEntity> reservationRepository = uow.getReservation();
		
		if (reservationRepository.stream()
				.anyMatch(e -> e.isReserve(vehicule, startDate, endDate))) {
			throw new ValidationException("Le véhicule n'est pas disponibles pour les dates et heures spécifiés,");
		}
		
		ReservationEntity reservation = optionalReservation.get();
		reservation.setVehicule(vehicule);
		reservation.setStartDate(startDate);
		reservation.setEndDate(endDate);
		
		uow.saveChanges();
	}
	
	/**
	 * Supprime une réservation par son numéro de réservation.
	 * @param noReservation Numéro de réservation
	 * @throws Exception Un erreur est survenue lors de la suppression de la r�servation.
	 */
	public void deleteReservation(int noReservation) throws Exception {
		Optional<ReservationEntity> optionalReservation = uow.getReservation().stream()
				.filter(e -> e.getNoReservation() == noReservation)
				.findFirst();

		if (optionalReservation.isEmpty()) {
			throw new ValidationException("La réservation avec le numéro de réservation spécifié n'existe pas.");
		}
		
		ReservationEntity reservation = optionalReservation.get();
		uow.getReservation().remove(reservation);
		uow.saveChanges();
	}
	
	/**
	*  Demarre la location et retour de location
	*  
	*  Par Pierre Tremblay
	*/
	
    /**
     * Permet d'effectuer le debut de location en injectant une date de d�part 
     * 
     * @param int
     * @throws Exception
     */  
	public void setLocation(int noReservation) throws Exception {
		Optional<ReservationEntity> optionalReservation = uow.getReservation().stream()
				.filter(e -> e.getNoReservation() == noReservation)
				.findFirst();
		
		if (optionalReservation.isPresent()) {
			ReservationEntity reservation = optionalReservation.get();
			LocationEntity location = new LocationEntity();
			location.setStartDate(LocalDateTime.now());
			reservation.setLocation(location);
		}
		
		uow.saveChanges();
	}
	
	
    /**
     * Permet d'effectuer de retour de location en injectant une date de retour de location, mettre
     * a jour le km du vehicule et mettre un cout a la facture. 
     * 
     * @param int, int, float
     * @throws Exception
     */  
	public void setRetourLocation(int noReservation, int kilometrage, float fCout) throws Exception {
		Optional<ReservationEntity> optionalReservation = uow.getReservation().stream()
				.filter(e -> e.getNoReservation() == noReservation)
				.findFirst();
		
		if (optionalReservation.isPresent()) {
			ReservationEntity reservation = optionalReservation.get();
			reservation.setCout(fCout);
			
			LocationEntity location = reservation.getLocation();
			RetourEntity retour = new RetourEntity();
			retour.setDate(LocalDateTime.now());
			retour.setKilometrage(kilometrage);
			location.setRetour(retour);
		}
		
		uow.saveChanges();
	}
	
	
}