package inf1007.bll.facades;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.stream.Collectors;

import inf1007.core.dtos.ActiviteJournaliereDto;
import inf1007.core.dtos.RapportDto;
import inf1007.core.dtos.VehiculeDto;
import inf1007.core.entities.RapportEntity;
import inf1007.core.entities.TypeVehiculeEntity;
import inf1007.core.interfaces.UnitOfWork;

/**
 * Facade des logiques d'affaires du rapport. 
 * @author Mathis Benny
 *
 */
public class RapportFacade {
	private UnitOfWork uow;
	
	public RapportFacade(UnitOfWork uow) {
		this.uow = uow;
	}
	
	/**
	 * Obtient les v�hicules hors-service, c'est-�-dire qu'ils sont retir�s.
	 * 
	 * @return Une collection de v�hicules.
	 */
	private Collection<VehiculeDto> getVehiculesHorsService() {
		return uow.getVehicule().stream()
			.filter(e -> !e.isEnService())
			.map(e -> e.toDto())
			.collect(Collectors.toList());
	}
	
	/**
	 * Obtient les v�hicules en retard.
	 * 
	 * @return Une collection de v�hicules.
	 */
	private Collection<VehiculeDto> getVehiculesEnRetard() {
		return uow.getReservation().stream()
			.filter(e -> e.isEnRetard())
			.map(e -> e.getVehicule().toDto())
			.collect(Collectors.toList());
	}
	
	/**
	 * Obtient tous les v�hicules.
	 * 
	 * @return Une collection de v�hicules.
	 */
	private Collection<VehiculeDto> getVehicules() {
		return uow.getVehicule().stream()
			.map(e -> e.toDto())
			.collect(Collectors.toList());
	}
	
	/**
	 * Obtient une activitée journalière selon le type de véhicule en paramètre.
	 * 
	 * @return Une instance d'activité journalière.
	 */
	
	private ActiviteJournaliereDto createActiviteJournaliere(TypeVehiculeEntity typeVehicule) {
		LocalDate today = LocalDate.now();
		
		long sortie =  uow.getReservation().stream()
				.filter(e -> e.getVehicule().getTypeVehicule() == typeVehicule 
						&& e.getLocation() != null)
				.map(e -> e.getLocation())
				.filter(e -> e.getStartDate().toLocalDate().equals(today))
				.count();
		
		long entree = uow.getReservation().stream()
				.filter(e -> e.getVehicule().getTypeVehicule() == typeVehicule
						&& e.getLocation() != null 
						&& e.getLocation().getRetour() != null)
				.map(e -> e.getLocation().getRetour())
				.filter(e -> e.getDate().toLocalDate().equals(today))
				.count();
		
		ActiviteJournaliereDto dto = new ActiviteJournaliereDto();
		
		dto.setEntree(entree);
		dto.setSortie(sortie);
		dto.setTypeVehicule(typeVehicule.toDto());
		
		return dto;			
	}
	/**
	 * Crée un rapport et l'ajoute à l'Unit of Work.
	 * 
	 * @throws Exception
	 */
	public RapportDto createRapport() throws Exception {
		RapportEntity rapport = new RapportEntity();
		rapport.setVehicules(getVehicules());
		rapport.setVehiculesHorsService(getVehiculesHorsService());
		rapport.setVehiculesEnRetard(getVehiculesEnRetard());
		rapport.setDate(LocalDateTime.now());
		
		Collection<ActiviteJournaliereDto> dtos = uow.getTypeVehicule().stream()
			.map(e -> createActiviteJournaliere(e))
			.collect(Collectors.toList());
		
		rapport.setActivitesJournalieres(dtos);
		
		uow.saveChanges();
		
		return rapport.toDto();
	}
}
