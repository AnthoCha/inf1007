package inf1007.bll.facades;

import java.time.LocalDate;
import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

import inf1007.core.dtos.VehiculeDto;
import inf1007.core.entities.TypeVehiculeEntity;
import inf1007.core.entities.VehiculeEntity;
import inf1007.core.enums.Classification;
import inf1007.core.exceptions.ValidationException;
import inf1007.core.interfaces.UnitOfWork;

/**
 * Facade des logiques d'affaires des véhicules.
 * 
 * @author Anthony Chamberland
 * @author Manu Magre
 *
 */
public class VehiculeFacade {
	private UnitOfWork uow;

	public VehiculeFacade(UnitOfWork uow) {
		this.uow = uow;
	}

	/**
	 * Obtient les véhicules en service, c'est-à-dire qu'ils ne sont pas retirés.
	 * 
	 * @return Une collection de véhicules.
	 */
	public Collection<VehiculeDto> getVehiculesEnService() {
		return uow.getVehicule().stream()
				.filter(e -> e.isEnService())
				.map(e -> e.toDto())
				.collect(Collectors.toList());
	}

	/**
	 * Obtient les véhicules en service, c'est-à-dire qu'ils ne sont pas retirés qui
	 * contient le numéro de location précisé.
	 * 
	 * @return Une collection de véhicules.
	 */
	public Collection<VehiculeDto> findVehiculesEnService(String noLocation) {
		return uow.getVehicule().stream()
				.filter(e -> e.getNoLocation().contains(noLocation) &&
						e.isEnService())
				.map(e -> e.toDto())
				.collect(Collectors.toList());
	}
	
	/**
	 * Cr�e une instance de v�hiculeEntity avec les bonnes donn�es
	 * et la sauvegarde dans uow
	 * 
	 * @param noLocation
	 * @param registration
	 * @param classification
	 * @param mileage
	 * @param color
	 * @param model
	 * @param brand
	 * @param year
	 * @throws Exception
	 */
	
	public void CreateVehicule(String noLocation, String registration, Classification classification, int mileage,
			String color, String model, String brand, int year) throws Exception{
		noLocation = noLocation.trim();
		registration = registration.trim();
		color = color.trim();
		model = model.trim();
		brand = brand.trim();
		
		if(noLocation.isEmpty()) {
			throw new ValidationException("Le numéro de location est requis.");
		}
		if(registration.isEmpty()) {
			throw new ValidationException("Le numéro de plaque est requis.");
		}
		if(classification == null) {
			throw new ValidationException("Le type classification est requis.");
		}
		if(model.isEmpty()) {
			throw new ValidationException("Le nom du modèle est requis.");
		}
		if(brand.isEmpty()) {
			throw new ValidationException("La marque est requise.");
		}
		if(year < 1950 || year > LocalDate.now().getYear()) {
			throw new ValidationException("L'année doit être entre 1950 et l'année actuelle exclusivement.");
		}
		
		Optional<TypeVehiculeEntity> typeVehicule = uow.getTypeVehicule().stream().findFirst();
		
		uow.getVehicule().add(new VehiculeEntity(noLocation, registration, classification, mileage, color, model, brand, year, typeVehicule.get()));
		
		uow.saveChanges();
	}

}
