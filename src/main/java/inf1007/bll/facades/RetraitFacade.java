package inf1007.bll.facades;

import inf1007.core.entities.RetraitEntity;
import inf1007.core.entities.VehiculeEntity;
import inf1007.core.interfaces.UnitOfWork;
import inf1007.core.enums.RaisonRetrait;
import inf1007.core.exceptions.ValidationException;

import java.time.LocalDateTime;
import java.util.Optional;

/**
 * Facade des logiques d'affaires des retraits.
 * 
 * @author Mathis Benny
 *
 */
public class RetraitFacade {
	private UnitOfWork uow;
	
	public RetraitFacade(UnitOfWork uow) {
		this.uow = uow;
	}
	
	/**
	 * Crée le retrait à l'aide d'un dto de véhicule pour gérer la mise de côté de ce dernier. 
	 * Gère également la validation de la création du retrait.
	 * 
	 * @param vehicule
	 * @param raison
	 * @param autre
	 * @param description
	 * @throws Exception
	 */
	public void createRetrait(String noLocation, RaisonRetrait raison, String autre, String description) throws Exception {
		Optional<VehiculeEntity> optionalVehicule = uow.getVehicule().stream()
				.filter(e -> e.getNoLocation().equals(noLocation))
				.findFirst();
		
		autre = autre.trim();
		description = description.trim();
		
		if(raison == null) {
			throw new ValidationException("La raison est requise");
		}
		
		if(raison == RaisonRetrait.AUTRE && autre.isEmpty()) {
			throw new ValidationException("Si le choix autre est sélectionné, le champs de texte autre est requis");
		}
		
		if(description.isEmpty()) {
			throw new ValidationException("La description est requise");
		}
		
		RetraitEntity retrait = new RetraitEntity();
		retrait.setAutre(autre);
		retrait.setRaisonRetrait(raison);
		retrait.setDescription(description);
		retrait.setDate(LocalDateTime.now());
		
		VehiculeEntity toBeRemovedVehicule = optionalVehicule.get();
		toBeRemovedVehicule.setRetrait(retrait);
		
		uow.saveChanges();
	}
}