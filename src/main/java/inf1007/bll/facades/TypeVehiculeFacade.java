package inf1007.bll.facades;

import inf1007.core.entities.TypeVehiculeEntity;
import inf1007.core.interfaces.UnitOfWork;
import inf1007.core.exceptions.ValidationException;

/**
 * Facade des logiques d'affaires des types de véhicule.
 * 
 * @author Mathis Benny
 *
 */
public class TypeVehiculeFacade {
	private UnitOfWork uow;
	
	public TypeVehiculeFacade(UnitOfWork uow) {
		this.uow = uow;
	}
	
	/**
	 * Crée le type de véhicule selon l'entity de base.
	 * Effectue la validation du nouveau type de véhicule.
	 * 
	 * @param coteClassification
	 * @param nom
	 * @throws Exception
	 */
	public void createTypeVehicule(String coteClassification, String nom) throws Exception {
		coteClassification = coteClassification.trim();
		nom = nom.trim();
		
		if(coteClassification.isEmpty()) {
			throw new ValidationException("La cote de classification est requise");
		}
		
		if(nom.isEmpty()) {
			throw new ValidationException("Le nom est requis");
		}
		
		TypeVehiculeEntity typeVehicule = new TypeVehiculeEntity();
		typeVehicule.setCoteClassification(coteClassification);
		typeVehicule.setNom(nom);
		
		uow.getTypeVehicule().add(typeVehicule);
		uow.saveChanges();
	}
}