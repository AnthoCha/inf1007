package inf1007.bll;

import inf1007.bll.facades.ClientFacade;
import inf1007.bll.facades.RapportFacade;
import inf1007.bll.facades.ReservationFacade;
import inf1007.bll.facades.RetraitFacade;
import inf1007.bll.facades.TypeVehiculeFacade;
import inf1007.bll.facades.UtilisateurFacade;
import inf1007.bll.facades.VehiculeFacade;
import inf1007.core.interfaces.SecurityProvider;
import inf1007.core.interfaces.UnitOfWork;

/**
 * Contexte des logiques d'affaires.
 * @author Anthony Chamberland
 * @author Mathis Benny
 * @implNote Cette impl�mentation offre un lazy loading des facades avec le m�me UnitOfWork.
 */
public class Context {
	private UnitOfWork uow;
	private SecurityProvider security;
	
	private UtilisateurFacade utilisateur;
	private ClientFacade client;
	private VehiculeFacade vehicule;
	private ReservationFacade reservation;
	private RapportFacade rapport;
	private RetraitFacade retrait;
	private TypeVehiculeFacade typeVehicule;
	
	public Context(UnitOfWork uow, SecurityProvider security) {
		this.uow = uow;
		this.security = security;
	}
	
	public UtilisateurFacade getUtilisateur() {
		if (utilisateur == null) {
			utilisateur = new UtilisateurFacade(uow, security);
		}
		
		return utilisateur;
	}
	
	public ClientFacade getClient() {
		if (client == null) {
			client = new ClientFacade(uow, security);
		}
		
		return client;
	}
	
	public VehiculeFacade getVehicule() {
		if (vehicule == null) {
			vehicule = new VehiculeFacade(uow);
		}
		
		return vehicule;
	}
	
	public ReservationFacade getReservation() {
		if (reservation == null) {
			reservation = new ReservationFacade(uow);
		}
		
		return reservation;
	}
	
	public RapportFacade getRapport() {
		if (rapport == null) {
			rapport = new RapportFacade(uow);
		}
		
		return rapport;
	}
	
	public RetraitFacade getRetrait() {
		if (retrait == null) {
			retrait = new RetraitFacade(uow);
		}
		
		return retrait;
	}
	
	public TypeVehiculeFacade getTypeVehicule() {
		if (typeVehicule == null) {
			typeVehicule = new TypeVehiculeFacade(uow);
		}
		
		return typeVehicule;
	}
}
