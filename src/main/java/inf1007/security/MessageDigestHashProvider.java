package inf1007.security;

import java.security.MessageDigest;

import inf1007.core.interfaces.HashProvider;

/**
 * Fournisseur de hachage à partir d'un {@link MessageDigest}.
 * @author Anthony Chamberland
 *
 */
public class MessageDigestHashProvider implements HashProvider {
	private MessageDigest digest;
	
	public MessageDigestHashProvider(MessageDigest digest) {
		this.digest = digest;
	}
	
	@Override
	public byte[] hash(byte[] bytes) {
		return digest.digest(bytes);
	}

}
