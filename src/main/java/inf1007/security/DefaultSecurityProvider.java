package inf1007.security;

import java.nio.charset.Charset;
import java.util.Arrays;

import inf1007.core.interfaces.HashProvider;
import inf1007.core.interfaces.RandomProvider;
import inf1007.core.interfaces.SecurityProvider;
import inf1007.util.ByteArrayConcatener;

/**
 * Fournisseur de sécurité à partir d'inversion de dépendance par défaut.
 * @author Anthony Chamberland
 *
 */
public class DefaultSecurityProvider implements SecurityProvider {
	private HashProvider hash;
	private RandomProvider random;
	private int saltLength;
	private Charset charset;
	
	public DefaultSecurityProvider(HashProvider hash, RandomProvider random, int saltLength, Charset charset) {
		this.hash = hash;
		this.random = random;
		this.saltLength = saltLength;
		this.charset = charset;
	}
	
	@Override
	public byte[] getSalt() {
		byte[] salt = new byte[saltLength];
		random.nextBytes(salt);
		return salt;
	}

	@Override
	public byte[] getHashWithSalt(String value, byte[] salt) {
		byte[] bytes = value.getBytes(charset);
		ByteArrayConcatener concatener = new ByteArrayConcatener();
		return hash.hash(concatener.concat(bytes, salt));
	}

	@Override
	public boolean validateHashWithSalt(String value, byte[] salt, byte[] hash) {
		byte[] bytes = getHashWithSalt(value, salt);
		return Arrays.equals(bytes, hash);
	}

}
