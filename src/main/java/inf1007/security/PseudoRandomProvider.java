package inf1007.security;

import java.util.Random;

import inf1007.core.interfaces.RandomProvider;

/**
 * Fournisseur pseudo aléatoire.
 * @author Anthony Chamberland
 *
 */
public class PseudoRandomProvider implements RandomProvider {
	Random pseudoRandom;
	
	public PseudoRandomProvider() {
		pseudoRandom = new Random();
	}
	
	@Override
	public void nextBytes(byte[] bytes) {
		pseudoRandom.nextBytes(bytes);
	}

}
