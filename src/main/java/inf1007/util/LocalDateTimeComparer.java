package inf1007.util;

import java.time.LocalDateTime;

public class LocalDateTimeComparer {
	/**
	 * �value si une date est entre deux dates pr�cis�s excluant les bornes.
	 * @param value Date
	 * @param min Borne minimale
	 * @param max Borne maximal
	 * @return true si la date est entre les deux dates exluant les bornes, false sinon.
	 */
	public boolean isBetween(LocalDateTime value, LocalDateTime min, LocalDateTime max) {
		return value.isAfter(min) && value.isBefore(max);
	}
}
