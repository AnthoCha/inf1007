package inf1007.util.stream;

import java.util.stream.LongStream;

public class StepLongStream {
	/**
	 * Obtient un flux d'entiers entre les bornes sp�cifi�s avec les sauts sp�cifi�s.
	 * @param startInclusive Borne minimale inclusive
	 * @param endExclusive Borne maximale exclusive
	 * @param step Sauts
	 * @return Un flux d'entiers
	 */
	public static LongStream range(long startInclusive, long endExclusive, long step) {
        return LongStream.range(startInclusive - startInclusive, (endExclusive - startInclusive) / step).map(i -> i * step + startInclusive);
    }
	
}
