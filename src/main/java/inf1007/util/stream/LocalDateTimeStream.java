package inf1007.util.stream;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.stream.Stream;

public class LocalDateTimeStream {
	/**
	 * Obtient un flux de dates et heures entre les dates et heures sp�cifi�s avec des sauts de la dur�e sp�cifi�e.
	 * @param startInclusive Borne minimale inclusive
	 * @param endExclusive Borne maximale exclusive
	 * @param stepDuration Dur�e des sauts
     * @throws IllegalArgumentException La date et heure de fin est plus petite que la date et heure de d�but.
	 * @return Un flux de dates et heures
	 */
    public static Stream<LocalDateTime> range(LocalDateTime startInclusive, LocalDateTime endExclusive, Duration stepDuration) {
        long end = endExclusive.toEpochSecond(ZoneOffset.UTC);
        long start = startInclusive.toEpochSecond(ZoneOffset.UTC);
        if (end < start) {
            throw new IllegalArgumentException(endExclusive + " < " + startInclusive);
        }
        
        long step = stepDuration.toSeconds();
        return StepLongStream.range(start, end, step).mapToObj(i -> LocalDateTime.ofEpochSecond(i, 0, ZoneOffset.UTC));
    }
}
