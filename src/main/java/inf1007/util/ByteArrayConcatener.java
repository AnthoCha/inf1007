package inf1007.util;

public class ByteArrayConcatener {
	public byte[] concat(byte[] a, byte[] b) {
		byte[] array = new byte[a.length + b.length];
		int i = 0;
		
		for (byte elem : a) {
			array[i++] = elem;
		}
		
		for (byte elem : b) {
			array[i++] = elem;
		}
		
		return array;
	}
}
