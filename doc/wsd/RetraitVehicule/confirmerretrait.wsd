@startuml

title Retrait véhicule: confirmer le retrait

participant ": DeleteVehiculeController" as DVC
participant "context: Context" as C
participant "retrait: RetraitFacade" as RF
participant "uow: UnitOfWork" as UoW
participant "vehicule: Repository<VehiculeEntity>" as VR
participant ": Repository<RetraitEntity>" as RR
participant ": RetraitEntity" as RE
participant ": VehiculeEntity" as VE

note left UoW
  Les prochains cas d'ob partagent les mêmes
  patterns, selon quoi les annotations
  seront absentes pour alléger la
  lecture.
end note

[-> DVC : createRetrait()
activate DVC
DVC -> C : getRetrait()
note right DVC
  Par Expert:
  Context contient 
  les facades
end note
activate C
note right RF
  Patron facade:
  Pour séparer la couche BD de 
  la partie vue/controleur
 end note
group opt [retrait == null]
  C -> RF ** : retrait = create(uow: UnitOfWork)
  note right C
    Par Créateur:
    Context contient la facade
  end note
  activate RF
  RF -> C : return RetraitFacade
  deactivate RF
end
C -> DVC : return RetraitFacade
deactivate C

DVC -> RF : createRetrait(noLocation: String, raison: RaisonRetrait, autre: String, description: String)
activate RF

RF -> UoW : getVehicule()
activate UoW
note right RF
  Par Expert:
  Repository contient
  la collection de 
  VehiculeEntity
end note
UoW -> RF : return Repository<VehiculeEntity>
deactivate UoW

RF -> VR : stream()
note right RF
  Par Expert:
  Stream<VehiculeEntity>
  contient le véhicule recherché.
end note
activate VR
VR -> RF : return Stream<VehiculeEntity>
deactivate

group filter
  RF -> VE : getNoLocation()
  activate VE
  VE -> RF : return String
  deactivate VE
end

RF -> RE ** : create()
note right RF 
  Par Créateur:
  La facade est responsable
  de créer les objets du domaines.
end note
activate RE
RE -> RF : return RetraitEntity
deactivate

RF -> RE : setAutre(autre: String)
activate RE
RE -> RF : return void
deactivate

RF -> RE : setRaisonRetrait(raison: RaisonRetrait)
activate RE
RE -> RF : return void
deactivate

RF -> RE : setDescription(description: String)
activate RE
RE -> RF : return void
deactivate

RF -> RE : setDate(date: LocalDateTime)
activate RE
RE -> RF : return void
deactivate

RF -> VE : setRetrait(retrait: RetraitEntity)
activate VE
deactivate VE

RF -> UoW : saveChanges()
activate UoW
deactivate UoW
deactivate RF
deactivate DVC

@enduml