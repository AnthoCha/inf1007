package inf1007.tests.core.entities;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;
import java.time.LocalDateTime;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import inf1007.core.entities.ReservationEntity;
import inf1007.core.entities.VehiculeEntity;

public class ReservationEntityTest {
	private ReservationEntity entity;
	private VehiculeEntity vehicule;
	private LocalDateTime startDate, endDate, dateTimeBefore, dateTimeBetween, dateTimeAfter;
	private LocalDate dateBefore, dateBetween, dateAfter;
	
	@BeforeEach
	public void setUp() {
		vehicule = new VehiculeEntity();
		startDate = LocalDateTime.of(2000, 1, 2, 12, 0);
		endDate = LocalDateTime.of(2000, 1, 4, 12, 0);
		entity = new ReservationEntity(vehicule, startDate, endDate);
		
		dateTimeBefore = LocalDateTime.of(2000, 1, 2, 8, 0);
		dateTimeBetween = LocalDateTime.of(2000, 1, 3, 12, 0);
		dateTimeAfter = LocalDateTime.of(2000, 1, 4, 16, 0);
		
		dateBefore = LocalDate.of(2000, 1, 1);
		dateBetween = LocalDate.of(2000, 1, 3);
		dateAfter = LocalDate.of(2000, 1, 5);
	}
	
	// isReserve_withDateTime
	
	@Test
	public void test_isReserve_withDateTimeBefore_returnsFalse() {
		boolean isReserve = entity.isReserve(vehicule, dateTimeBefore);

		assertFalse(isReserve);
	}
	
	@Test
	public void test_isReserve_withDateTimeAfter_returnsFalse() {
		boolean isReserve = entity.isReserve(vehicule, dateTimeAfter);

		assertFalse(isReserve);
	}
	
	@Test
	public void test_isReserve_withDateTimeBetween_returnsTrue() {
		boolean isReserve = entity.isReserve(vehicule, dateTimeBetween);

		assertTrue(isReserve);
	}
	
	@Test
	public void test_isReserve_withDifferentVehiculeAndDateTimeBetween_returnsFalse() {
		VehiculeEntity differentVehicule = new VehiculeEntity();
		
		boolean isReserve = entity.isReserve(differentVehicule, dateTimeBetween);

		assertFalse(isReserve);
	}
	
	@Test
	public void test_isReserve_withStartDate_returnsFalse() {
		boolean isReserve = entity.isReserve(vehicule, startDate);

		assertFalse(isReserve);
	}
	
	@Test
	public void test_isReserve_withEndDate_returnsFalse() {
		boolean isReserve = entity.isReserve(vehicule, endDate);

		assertFalse(isReserve);
	}
	
	// isReserve_withDate
	
	@Test
	public void test_isReserve_withDateBefore_returnsFalse() {
		boolean isReserve = entity.isReserve(vehicule, dateBefore);

		assertFalse(isReserve);
	}
	
	@Test
	public void test_isReserve_withDateAfter_returnsFalse() {
		boolean isReserve = entity.isReserve(vehicule, dateAfter);

		assertFalse(isReserve);
	}
	
	@Test
	public void test_isReserve_withDateBetween_returnsTrue() {
		boolean isReserve = entity.isReserve(vehicule, dateBetween);

		assertTrue(isReserve);
	}
	
	@Test
	public void test_isReserve_withDifferentVehiculeAndDateBetween_returnsFalse() {
		VehiculeEntity differentVehicule = new VehiculeEntity();
		
		boolean isReserve = entity.isReserve(differentVehicule, dateBetween);

		assertFalse(isReserve);
	}
	
	@Test
	public void test_isReserve_withDateDuringStartDate_returnsFalse() {
		boolean isReserve = entity.isReserve(vehicule, startDate.toLocalDate());

		assertFalse(isReserve);
	}
	
	@Test
	public void test_isReserve_withDateDuringEndDate_returnsFalse() {
		boolean isReserve = entity.isReserve(vehicule, endDate.toLocalDate());

		assertFalse(isReserve);
	}
	
	// isReserve_withDateTimeToDateTime
	
	@Test
	public void test_isReserve_withDateTimeBeforeToStartDate_returnsFalse() {
		boolean isReserve = entity.isReserve(vehicule, dateTimeBefore, startDate);

		assertFalse(isReserve);
	}
	
	@Test
	public void test_isReserve_withEndDateToDateTimeAfter_returnsFalse() {
		boolean isReserve = entity.isReserve(vehicule, endDate, dateTimeAfter);

		assertFalse(isReserve);
	}
	
	@Test
	public void test_isReserve_withStartDateToDateTimeBetween_returnsTrue() {
		boolean isReserve = entity.isReserve(vehicule, startDate, dateTimeBetween);

		assertTrue(isReserve);
	}
	
	@Test
	public void test_isReserve_withDateTimeBetweenToEndDate_returnsTrue() {
		boolean isReserve = entity.isReserve(vehicule, dateTimeBetween, endDate);

		assertTrue(isReserve);
	}
	
	@Test
	public void test_isReserve_withDateTimeBetweenToDateTimeBetween_returnsTrue() {
		boolean isReserve = entity.isReserve(vehicule, dateTimeBetween, dateTimeBetween);

		assertTrue(isReserve);
	}
	
	@Test
	public void test_isReserve_withStartDateToEndDate_returnsTrue() {
		boolean isReserve = entity.isReserve(vehicule, startDate, endDate);

		assertFalse(isReserve);
	}
	
	@Test
	public void test_isReserve_withDifferentVehiculeAndDateTimeBetweenToDateTimeBetween_returnsFalse() {
		VehiculeEntity differentVehicule = new VehiculeEntity();
		
		boolean isReserve = entity.isReserve(differentVehicule, dateTimeBetween, dateTimeBetween);

		assertFalse(isReserve);
	}

}
